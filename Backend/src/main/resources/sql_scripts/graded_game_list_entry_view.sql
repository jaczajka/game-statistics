DO
$do$
    BEGIN

        if not exists(select from information_schema.views where table_name = 'graded_game_list_entry') then
            drop table if exists graded_game_list_entry;
        end if;

        create or replace view graded_game_list_entry as
        select title_id,
               genre_id,
               title.name                        as title,
               g2.name                           as genre,
               cs.score_issuer_client_id         as client_id,
               cs.value                          as given_score,
               cs.scored_game_game_id            as game_id,
               p.name                            as platform,
               string_agg(distinct d.name, ', ') as developers
        from title
                 left join game g on title.title_id = g.title_title_id
                 left join genre g2 on title.genre_genre_id = g2.genre_id
                 left join game_developers gd on g.game_id = gd.games_game_id
                 left join client_score cs on g.game_id = cs.scored_game_game_id
                 left join platform p on g.platform_platform_id = p.platform_id
                 left join developer d on gd.developers_developer_id = d.developer_id
        group by title_id, genre_id, client_id, given_score, cs.scored_game_game_id, p.name;
    END
$do$
