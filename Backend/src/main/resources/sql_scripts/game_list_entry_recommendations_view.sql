DO
$do$
BEGIN


if not exists (select pe.extname from pg_extension pe where pe.extname = 'pg_trgm') then
    create extension pg_trgm;
end if;

if not exists(select from information_schema.views where table_name = 'clients_games_preferability') then
    drop table if exists clients_games_preferability;
end if;

create or replace view clients_games_preferability as
    select cs.score_issuer_client_id, cs.scored_game_game_id, cs.value from client_score cs
    union
    select ft.favourite_of_client_id as score_issuer_client_id,
           g.game_id as scored_game_game_id,
           10 --max_score
    from client_favourite_titles ft
    inner join game g on g.title_title_id = ft.favourite_titles_title_id;

if not exists(select from information_schema.views where table_name = 'title_preference_coefficients') then
    drop table if exists title_preference_coefficients;
end if;

create or replace view title_preference_coefficients as
select
        t.title_id as title_id,
        t.name as title_name,
        c.client_id as client_id,
        c.name as client_name,
        avg(similarity(cast(t.name as text), cast(coalesce(cgpt.name, '') as text))  * ( coalesce(cgp.value - 5, 0.0))) as title_similarity_preferability, --title name similarity multiplied by score
        avg((cast(t.genre_genre_id = coalesce(cgpt.genre_genre_id, 0) as int)) * coalesce(cgp.value - 5, 0.0)) as genre_equality_preferability, --genre similarity multiplied by score
        avg((cast((t.publisher_publisher_id = coalesce(cgpt.publisher_publisher_id, 0)) as int)) * coalesce(cgp.value - 5, 0.0)) as publisher_equality_preferability,
        cast(cft.favourite_of_client_id is not null as int) as presence_in_favourite_titles,
        cast(cpg.preferred_genres_genre_id is not null as int) as presence_in_preferred_ganres
    from client c
    cross join title t
    inner join game g on g.title_title_id = t.title_id
    --match title from favourite title, if user does not specified this title in favourites we know that because null was returned
    left join client_favourite_titles cft on c.client_id = cft.favourite_of_client_id and t.title_id = cft.favourite_titles_title_id
    --match genre from preferred genres, if user does not specified this genre in preferred we know that because null was returned
    left join client_preferred_genres cpg on c.client_id = cpg.preferred_by_client_id and t.genre_genre_id = cpg.preferred_genres_genre_id
    --left join client scores on all games to check if those games match users issued scores
    left join clients_games_preferability cgp on c.client_id = cgp.score_issuer_client_id
    left join game cgpg on cgpg.game_id = cgp.scored_game_game_id
    left join title cgpt on cgpg.title_title_id = cgpt.title_id
    --match favourite title to given client scores
group by t.title_id, t.name, c.name, c.client_id, cft.favourite_of_client_id, cpg.preferred_genres_genre_id;

if not exists(select from information_schema.views where table_name = 'title_preferability') then
    drop table if exists title_preferability;
end if;

create or replace view title_preferability as
    select
        tpc.title_id as title_id,
        tpc.title_name as title_name,
        tpc.client_id as client_id,
        tpc.client_name as client_name,
            0.25 * tpc.title_similarity_preferability +
            0.2 * tpc.genre_equality_preferability +
            0.15 * tpc.publisher_equality_preferability +
            0.4 * tpc.presence_in_preferred_ganres
        as preferencee_metric
    from title_preference_coefficients tpc;

if not exists(select from information_schema.views where table_name = 'game_list_recommendation_entry') then
    drop table if exists game_list_recommendation_entry;
end if;

create or replace view game_list_recommendation_entry as
select title.title_id,
       genre_id,
       title.name                        as title,
       g2.name                           as genre,
       avg(g.users_count)                as average_users_count,
       avg(g.critics_count)              as average_critics_count,
       avg(g.users_score)                as average_users_score,
       avg(g.critics_score)              as average_critics_score,
       string_agg(distinct d.name, ', ') as developers,
       tp.client_id                      as client_id,
       tp.client_name                    as client_name,
       tp.preferencee_metric             as preferability
from title
         left join game g on title.title_id = g.title_title_id
         left join genre g2 on title.genre_genre_id = g2.genre_id
         left join game_developers gd on g.game_id = gd.games_game_id
         left join developer d on gd.developers_developer_id = d.developer_id
         inner join title_preferability tp on tp.title_id = title.title_id
where not exists (select * from clients_games_preferability cgp
    inner join game g on g.game_id = cgp.scored_game_game_id
    inner join title t on t.title_id = g.game_id
    where t.title_id = title.title_id and cgp.score_issuer_client_id = tp.client_id
    )
group by title.title_id, genre_id, title.name, g2.name, tp.preferencee_metric, tp.client_id, tp.client_name
order by tp.preferencee_metric desc;

END
$do$