DO
$do$
    BEGIN

        if not exists(select from information_schema.views where table_name = 'favourite_game_list_entry') then
            drop table if exists favourite_game_list_entry;
        end if;

        create or replace view favourite_game_list_entry as
        select title_id,
               genre_id,
               t.name                            as title,
               g2.name                           as genre,
               avg(g.users_count)                as average_users_count,
               avg(g.critics_count)              as average_critics_count,
               avg(g.users_score)                as average_users_score,
               avg(g.critics_score)              as average_critics_score,
               cft.favourite_of_client_id        as client_id,
               string_agg(distinct d.name, ', ') as developers
        from title t
                 left join game g on t.title_id = g.title_title_id
                 left join genre g2 on t.genre_genre_id = g2.genre_id
                 left join game_developers gd on g.game_id = gd.games_game_id
                 left join client_favourite_titles cft on t.title_id = cft.favourite_titles_title_id
                 left join developer d on gd.developers_developer_id = d.developer_id
        group by title_id, genre_id, client_id;
    END
$do$