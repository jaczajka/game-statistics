DO
$do$
BEGIN

if not exists(SELECT * FROM INFORMATION_SCHEMA.table_constraints WHERE CONSTRAINT_NAME ='genre_unique') then
    ALTER TABLE genre ADD CONSTRAINT genre_unique UNIQUE (name);
end if;

if not exists(SELECT * FROM INFORMATION_SCHEMA.table_constraints WHERE CONSTRAINT_NAME ='publisher_unique') then
    ALTER TABLE publisher ADD CONSTRAINT publisher_unique UNIQUE (name);
end if;

if not exists(SELECT * FROM INFORMATION_SCHEMA.table_constraints WHERE CONSTRAINT_NAME ='developer_unique') then
    ALTER TABLE developer ADD CONSTRAINT developer_unique UNIQUE (name);
end if;

if not exists(SELECT * FROM INFORMATION_SCHEMA.table_constraints WHERE CONSTRAINT_NAME ='title_unique') then
    ALTER TABLE title ADD CONSTRAINT title_unique UNIQUE (name);
end if;

if not exists(SELECT * FROM INFORMATION_SCHEMA.table_constraints WHERE CONSTRAINT_NAME ='platform_unique') then
    ALTER TABLE platform ADD CONSTRAINT platform_unique UNIQUE (name);
end if;

if not exists(SELECT * FROM INFORMATION_SCHEMA.table_constraints WHERE CONSTRAINT_NAME ='rating_unique') then
    ALTER TABLE rating ADD CONSTRAINT rating_unique UNIQUE (name);
end if;

if not exists(SELECT * FROM INFORMATION_SCHEMA.table_constraints WHERE CONSTRAINT_NAME ='client_name_unique') then
    ALTER TABLE client
    ADD CONSTRAINT client_name_unique UNIQUE(name);
end if;

if not exists(SELECT * FROM INFORMATION_SCHEMA.table_constraints WHERE CONSTRAINT_NAME ='client_score_unique_foreign_keys') then
    ALTER TABLE client_score
    ADD CONSTRAINT client_score_unique_foreign_keys UNIQUE(score_issuer_client_id, scored_game_game_id );
end if;

if not exists(SELECT * FROM INFORMATION_SCHEMA.table_constraints WHERE CONSTRAINT_NAME ='tag_name_unique') then
    ALTER TABLE tag_name
    ADD CONSTRAINT tag_name_unique UNIQUE(name);
end if;

if not exists(SELECT * FROM INFORMATION_SCHEMA.table_constraints WHERE CONSTRAINT_NAME ='tag_unique_client_title_name') then
    ALTER TABLE tag
    ADD CONSTRAINT tag_unique_client_title_name UNIQUE(tag_name_tag_id, related_title_title_id, tag_issuer_client_id);
end if;


END
$do$
