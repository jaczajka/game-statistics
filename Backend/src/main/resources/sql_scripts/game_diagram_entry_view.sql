DO
$do$
BEGIN

if not exists(select from information_schema.views where table_name = 'game_diagram_entry') then
    drop table if exists game_diagram_entry;
end if;

create or replace view game_diagram_entry as
select  g.game_id,
        g.year_of_release       as year_of_release,
        g.critics_score         as critics_score,
        g.users_score           as users_score,
        g.north_america_sales   as north_america_sales,
        g.europe_sales          as europe_sales,
        g.japan_sales           as japan_sales,
        g.other_sales           as other_sales,
        g.global_sales          as global_sales,
        gen.name                as genre_name,
        gen.genre_id            as genre_id,
        r.name                  as rating_name,
        r.rating_id             as rating_id,
        p.name                  as platform_name,
        p.platform_id           as platform_id
from game g
    left join title t on t.title_id = g.title_title_id
    left join genre gen on t.genre_genre_id = gen.genre_id
    left join rating r on r.rating_id = g.rating_rating_id
    left join platform p on p.platform_id = g.platform_platform_id;

END
$do$
