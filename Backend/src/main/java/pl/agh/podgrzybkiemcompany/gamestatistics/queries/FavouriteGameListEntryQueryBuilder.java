package pl.agh.podgrzybkiemcompany.gamestatistics.queries;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.FavouriteGameListEntry;

import javax.persistence.EntityManager;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FavouriteGameListEntryQueryBuilder extends UserSpecificListQueryBuilder<FavouriteGameListEntry> {

    public FavouriteGameListEntryQueryBuilder(EntityManager entityManager, SortingOrderAdapter sortingAdapter) {
        super(entityManager, sortingAdapter, FavouriteGameListEntry.class, FavouriteGameListEntry.class);
    }
}
