package pl.agh.podgrzybkiemcompany.gamestatistics.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Client;

import java.util.ArrayList;
import java.util.Collection;

public class UserData implements UserDetails {

    private final String username;
    private final String password;

    public UserData(Client client) {
        this.username = client.getName();
        this.password = client.getHash();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new ArrayList<>();
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
