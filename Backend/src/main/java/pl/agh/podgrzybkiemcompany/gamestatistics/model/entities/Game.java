package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Setter
@Getter
@ToString(exclude = {"registeredClientsScores"})
@Entity
public class Game implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer gameId;
    private String yearOfRelease; //There are records that say N/A

    @NotNull
    private Double northAmericaSales;
    @NotNull
    private Double europeSales;
    @NotNull
    private Double japanSales;
    @NotNull
    private Double otherSales;
    @NotNull
    private Double globalSales;

    private Double criticsScore;
    private Integer criticsCount;
    private Double usersScore;
    private Integer usersCount;

    private static final long serialVersionUID = 123876129736L;

    @JsonIgnore
    @NotNull
    @ManyToOne
    private Title title;
    @NotNull
    @ManyToOne
    private Platform platform;
    @ManyToMany()
    private Set<Developer> developers = new HashSet<>();
    @ManyToOne
    private Rating rating;

    @JsonIgnore
    @OneToMany(mappedBy = "scoredGame")
    private Set<ClientScore> registeredClientsScores = new HashSet<>();
    public void addRegisteredClientsScore(ClientScore clientScore){
        registeredClientsScores.add(clientScore);
    }

    public void addDeveloper(Developer dev) {
        developers.add(dev);
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.title.getName(), this.platform.getName());
    }
}
