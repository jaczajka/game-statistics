package pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Rating;
@Repository
public interface RatingRepository extends CrudRepository<Rating, Integer> {
}
