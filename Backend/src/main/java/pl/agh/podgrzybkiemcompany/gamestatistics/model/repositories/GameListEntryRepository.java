package pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.dto.game.ListRepositoryExtension;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameListEntry;

public interface GameListEntryRepository extends JpaRepository<GameListEntry, Integer>, ListRepositoryExtension {
}
