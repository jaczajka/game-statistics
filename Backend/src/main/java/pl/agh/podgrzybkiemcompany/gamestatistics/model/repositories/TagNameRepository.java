package pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.TagName;

import java.util.Optional;

public interface TagNameRepository extends CrudRepository<TagName, Integer> {
    Optional<TagName> findByName(String tagName);
}
