package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString(exclude = {"games"})
@Entity
public class Developer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer developerId;

    @NotNull
    private String name;

    @JsonIgnore
    @ManyToMany(mappedBy = "developers")
    private Set<Game> games = new HashSet<>();

    public Developer() {
    }

    public Developer(String name) {
        this.name = name;
    }
}
