package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString()
@Entity
public class ClientScore {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer clientScoreId;

    @NotNull
    private Double value;

    @NotNull
    @JsonIgnore
    @ManyToOne
    private Client scoreIssuer;

    @NotNull
    @JsonIgnore
    @ManyToOne
    private Game scoredGame;

    public ClientScore(){}
    public ClientScore(Double value){
        this.value = value;
    }
}
