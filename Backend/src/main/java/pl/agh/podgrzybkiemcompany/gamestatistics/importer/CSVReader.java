package pl.agh.podgrzybkiemcompany.gamestatistics.importer;

import java.util.List;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class CSVReader {
    private static final String DEFAULT_SEPARATOR = ",";
    private static final int COLUMNS_NO = 16;

    private Column columnNameToEnum(String columnName){
        return Column.valueOf(columnName.toUpperCase());
    }

    public static void main(String[] args){
        for (HashMap record: getData()){
            System.out.println(record.get("Developer").getClass());
        }
    }


    public static List<HashMap> getData() {
        String path = "./src/main/resources/dane.csv";
        ArrayList<HashMap> result = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String line = reader.readLine();
            line = line.replaceAll(";$", "");
            String[] keys = line.split(DEFAULT_SEPARATOR);
            int i = 0;
            while ((line = reader.readLine()) != null) {
                line = line.replaceAll(";$", "");
                if (line.startsWith("\"")){
                    parseDoubleQuote(result, line, keys);
                } //napisac regexa - line.contains(".\\.?,.") ...
                else if (line.contains(", ") || line.contains("0,00") || line.contains("s,b") || line.contains("o,O")) {
                    parseSpecialNames(result, line, keys);
                }
                else {
                    parseNormal(result, line, keys);
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        unifyTitlesColumnData(result);
        unifyDevelopersColumnData(result);
        return result;
    }

    private static void unifyDevelopersColumnData(List<HashMap> data){
        for(HashMap record: data){
            //if developer is null, insert empty list
            if (record.get("Developer") == null){
                record.put("Developer", new ArrayList<>());//empty array list
            }
            else if(record.get("Developer") instanceof String){//if developer is string, insert one elem list
                record.put("Developer", new ArrayList<>(
                        List.of(
                                record.get("Developer")
                                        .toString()
                        ))
                );
            }
            else{
                ArrayList<String> developersList = (ArrayList<String>)record.get("Developer");
                long incRows = developersList
                        .stream()
                        .filter(elem -> elem.toLowerCase().contains("inc"))
                        .count();
                long ltdRows = developersList
                        .stream()
                        .filter(elem -> elem.toLowerCase().contains("ltd"))
                        .count();

                if (incRows + ltdRows > 0){
                    ArrayList<String> toInsert = new ArrayList<String>();
                    boolean specialMerged = false;
                    for (int i = 0; i < developersList.size(); i++){
                        if ( i < developersList.size() - 1 && !developersList.get(i).toLowerCase().contains("inc") &&
                                developersList.get(i+1).toLowerCase().contains("inc")){
                            //if this row does not contain "Inc" and if next row containg word "Inc", we have to merge current with next row
                            toInsert.add(developersList.get(i) + developersList.get(i+1));
                            specialMerged = true;//just skip next row
                        }
                        else if ( i < developersList.size() - 1 && !developersList.get(i).toLowerCase().contains("ltd") &&
                                developersList.get(i+1).toLowerCase().contains("ltd")){
                            //if this row does not contain "Inc" and if next row containg word "Inc", we have to merge current with next row
                            toInsert.add(developersList.get(i) + developersList.get(i+1));
                            specialMerged = true;//just skip next row
                        }
                        else{
                            if (!specialMerged){
                                toInsert.add(developersList.get(i));
                            }
                            specialMerged = false;
                        }
                    }


                    record.put("Developer", toInsert);
                }
            }
            //else leave it as it is
        }
    }

    private static void unifyTitlesColumnData(List<HashMap> data){
        for(HashMap record: data) {
            if (record.get("Title") != null) {
                String title = record.get("Title").toString();
                if (title.matches("^\"") || title.matches("^\'") || title.matches("^\\s")) {
                    record.put("Title", title.substring(1));
                }
            }
        }
    }
    private static void parseSpecialNames(ArrayList<HashMap> result, String line, String[] keys) {
        String[] split = new String[16];
        int splitIndex = 15;
        int lastIndex = line.length()-1;
        for (int i=lastIndex; i>=0; i--) {
            if (splitIndex == 0) {
                split[splitIndex] = line.substring(0, lastIndex + 1);
            } else {
                while (line.charAt(i) != ',') i--;

                split[splitIndex] = line.substring(i + 1, lastIndex + 1);
                splitIndex--;
                lastIndex = i - 1;
            }
        }
        HashMap<String, String> map = new HashMap<>();
        for (int i=0; i<16; i++) {
            if (split[i].equals(""))
                split[i] = null;
            map.put(keys[i], split[i]);
        }
        result.add(map);
    }

    private static void parseNormal(ArrayList<HashMap> result, String line, String[] keys) {
        String[] split = line.split(DEFAULT_SEPARATOR, COLUMNS_NO);
        HashMap<String, String> map = new HashMap<>();
        for (int i=0; i<split.length; i++) {
            if (split[i].equals(""))
                map.put(keys[i], null);
            else
                map.put(keys[i], split[i]);
        }
        result.add(map);
    }

    private static void parseDoubleQuote(ArrayList<HashMap> result, String line, String[] keys) {
        String[] split = line.split(DEFAULT_SEPARATOR);
        split = checkPublishersCorrectness(split);
        HashMap<String, Object> map = new HashMap<>();
        int offset = 0;
        for (int i=0; i<split.length; i++) {
            if (split[i].contains("\"\"")) {
                split[i] = split[i].replaceAll("\"\"", "");
                ArrayList<String> devs = new ArrayList<>();
                devs.add(split[i]);
                i++;
                while (!split[i].contains("\"\"")) {
                    i++;
                    devs.add(split[i]);
                }
                split[i] = split[i].replaceAll("\"\"", "");
                devs.add(split[i]);
                offset = devs.size() - 1;
                map.put(keys[i-offset], devs);
            } else if (i > 15) {
                split[i] = split[i].replaceAll("\"", "");
                map.put(keys[i-offset], split[i]);
            } else {
                map.put(keys[i], split[i]);
            }
        }
        result.add(map);
    }

    private static String[] checkPublishersCorrectness(String[] split) {
        if (split[4].contains("\"\"")) {
            split[4] = split[4].replaceAll("\"\"", "");
            split[5] = split[5].replaceAll("\"\"", "");
            split[4] = split[4] + "," + split[5];
            String[] newSplit = new String[split.length-1];
            for (int i=0; i<5; i++){
                newSplit[i] = split[i];;
            }
            for (int i=5; i<split.length - 1; i++){
                newSplit[i] = split[i + 1];;
            }
            return newSplit;
        }
        return split;
    }

}

