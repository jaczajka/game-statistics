package pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.dto.game.FavouriteGameListEntryRepositoryExtension;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.FavouriteGameListEntry;

public interface FavouriteGameListEntryRepository
        extends JpaRepository<FavouriteGameListEntry, Integer>, FavouriteGameListEntryRepositoryExtension {
}
