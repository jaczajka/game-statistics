package pl.agh.podgrzybkiemcompany.gamestatistics.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Genre;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Platform;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.GenreRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.PlatformRepository;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@RestController
public class GenresController {

    private final GenreRepository genreRepository;
    List<String> nonPermittedGenres = List.of("Not Known", "");

    @Autowired
    public GenresController(
            GenreRepository genreRepository
    ) {
        this.genreRepository = genreRepository;
    }


    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("games/genres")
    public Collection<Genre> platformListAction() {
        List<Genre> result = new LinkedList<>();
        genreRepository
                .findAll()
                .forEach(genre -> {
                    if (! nonPermittedGenres.contains(genre.getName())) {
                        result.add(genre);
                    }
                });

        return result;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("games/genres/names")
    public Collection<String> platformNamesListAction() {
        List<String> result = new LinkedList<>();
        genreRepository
                .findAll()
                .forEach(genre -> {
                    if (! nonPermittedGenres.contains(genre.getName())) {
                        result.add(genre.getName());
                    }
                });

        return result;
    }
}