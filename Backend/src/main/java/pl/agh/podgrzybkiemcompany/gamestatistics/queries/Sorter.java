package pl.agh.podgrzybkiemcompany.gamestatistics.queries;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import java.util.ArrayList;
import java.util.List;

public class Sorter {

    public static final String ASCENDING = "asc";

    private CriteriaBuilder queryBuilder;
    private List<Order> orderList = new ArrayList<>();

    private Sorter(CriteriaBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
    }

    public <T> Sorter addSortBy(String column, String direction, Path<T> from) {
        sortByFieldExpression(from.get(column), direction);
        return this;
    }

    private <T> void sortByFieldExpression(Expression<T> field, String direction) {
        orderList.add(direction.equals(ASCENDING)
                ? queryBuilder.asc(field)
                : queryBuilder.desc(field));
    }

    public List<Order> getOrder() {
        return this.orderList;
    }

    public static Sorter createForQuery(CriteriaBuilder queryBuilder) {
        return new Sorter(queryBuilder);
    }
}
