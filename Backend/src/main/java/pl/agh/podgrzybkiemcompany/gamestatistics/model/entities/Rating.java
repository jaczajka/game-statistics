package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString(exclude = {"games"})
@Entity
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer ratingId;

    @NotNull
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "rating")
    private Set<Game> games = new HashSet<>();

    public Rating() {
    }

    public Rating(String ratingName) {
        this.name = ratingName;
    }
}
