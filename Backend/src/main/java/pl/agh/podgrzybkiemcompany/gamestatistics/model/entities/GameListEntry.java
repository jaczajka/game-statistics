package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Immutable
@ToString
@Getter
@Setter
public class GameListEntry implements Serializable {

    private static final long serialVersionUID = 123876129736L;

    @Id
    private Integer titleId;
    private String title;
    private Integer genreId;
    private String genre;
    private String developers;
    private Integer averageUsersCount;
    private Integer averageCriticsCount;
    private Integer averageUsersScore;
    private Integer averageCriticsScore;
}
