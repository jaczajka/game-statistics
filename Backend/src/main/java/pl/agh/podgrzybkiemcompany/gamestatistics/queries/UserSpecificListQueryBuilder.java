package pl.agh.podgrzybkiemcompany.gamestatistics.queries;

import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Client;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;

public abstract class UserSpecificListQueryBuilder<T> extends PaginatedListQueryBuilder<T> {

    private static final String CLIENT_KEY_COLUMN_NAME = "clientId";
    private static final String USERNAME_COLUMN_NAME = "name";

    private final CriteriaBuilder criteriaBuilder;

    public UserSpecificListQueryBuilder(EntityManager entityManager, SortingOrderAdapter sortingAdapter, Class<T> resultType, Class<T> fromType) {
        super(entityManager, sortingAdapter, resultType, fromType);
        criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    public UserSpecificListQueryBuilder<T> filterByUsername(String username) {
        Predicate filter = createFilterByClientId(username);
        wheres.add(filter);
        return this;
    }

    private Predicate createFilterByClientId(String username) {
        Subquery<Client> subquery = createClientIdByUsernameQuery(username);
        Path<Object> path = from.get(CLIENT_KEY_COLUMN_NAME);
        return criteriaBuilder.in(path).value(subquery);
    }

    private Subquery<Client> createClientIdByUsernameQuery(String username) {
        CriteriaQuery<Object> query = criteriaBuilder.createQuery();
        Subquery<Client> subquery = query.subquery(Client.class);
        Root<Client> rootItem = subquery.from(Client.class);
        Path<Object> usernameFromDb = rootItem.get(USERNAME_COLUMN_NAME);
        return subquery.select(rootItem.get(CLIENT_KEY_COLUMN_NAME))
                .where(criteriaBuilder.equal(usernameFromDb, username));
    }
}
