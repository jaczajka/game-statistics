package pl.agh.podgrzybkiemcompany.gamestatistics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@ComponentScan("pl.agh.podgrzybkiemcompany.gamestatistics.services")
@ComponentScan("pl.agh.podgrzybkiemcompany.gamestatistics.importer")
@ComponentScan("pl.agh.podgrzybkiemcompany.gamestatistics.queries")
@ComponentScan("pl.agh.podgrzybkiemcompany.gamestatistics.listeners")
@SpringBootApplication
public class GameStatisticsApplication {
    public static void main(String[] args) {
        SpringApplication.run(GameStatisticsApplication.class, args);
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/games").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/annualGrades").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/diagrams").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/genres").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/genres/names").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/ratings").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/ratings/names").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/platforms").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/platforms/names").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/regions/names").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/client-scores").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/min-max-scores").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/favourites").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/favourites/ratio").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/favourites/fond-of-title").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/games/favourites/user-favourites-count").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/authenticate").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/register").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
                registry.addMapping("/tags").allowedOrigins("http://localhost:8081", "http://127.0.0.1:8081", "https://game-statistics-frontend.herokuapp.com");
            }
        };
    }
}
