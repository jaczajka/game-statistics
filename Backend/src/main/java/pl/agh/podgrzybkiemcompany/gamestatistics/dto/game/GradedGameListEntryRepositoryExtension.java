package pl.agh.podgrzybkiemcompany.gamestatistics.dto.game;

import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GradedGameListEntry;

public interface GradedGameListEntryRepositoryExtension
        extends DashboardGameListEntryRepositoryExtension<GradedGameListEntry> {
}
