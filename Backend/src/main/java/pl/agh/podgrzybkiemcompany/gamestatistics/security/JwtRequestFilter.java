package pl.agh.podgrzybkiemcompany.gamestatistics.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import pl.agh.podgrzybkiemcompany.gamestatistics.utils.JwtUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private UserAuthService userDetailsService;
    private static final String AUTH_HEADER_FIELD_NAME = "Authorization";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException
    {
        final String header = request.getHeader(AUTH_HEADER_FIELD_NAME);

        String jwt = null;
        String username = null;

        if (isValidHeader(header)) {
            jwt = extractToken(header);
            username = JwtUtils.extractUsername(jwt);
        }

        if (isUsernameSpecified(username))
            authenticate(request, jwt, username);
        filterChain.doFilter(request, response);
    }

    private boolean isValidHeader(final String header) {
        return header != null && header.startsWith("Bearer ");
    }

    private String extractToken(final String header) {
        return header.substring(7);
    }

    private boolean isUsernameSpecified(String username) {
        return username != null;
    }

    private void authenticate(HttpServletRequest request, String jwt, String username) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        if (notAlreadyAuthenticated() && JwtUtils.validateToken(jwt, userDetails))
            createTokenAndSetInSecurityContext(request, userDetails);
    }

    private void createTokenAndSetInSecurityContext(HttpServletRequest request, UserDetails userDetails) {
        var token = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        var details = new WebAuthenticationDetailsSource().buildDetails(request);
        token.setDetails(details);
        SecurityContextHolder.getContext().setAuthentication(token);
    }

    private boolean notAlreadyAuthenticated() {
        return SecurityContextHolder.getContext().getAuthentication() == null;
    }
}
