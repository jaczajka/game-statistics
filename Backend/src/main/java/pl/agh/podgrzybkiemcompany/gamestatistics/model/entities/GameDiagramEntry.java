package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Immutable
@Setter
@Getter
@ToString
public class GameDiagramEntry {

    private static final long serialVersionUID = 123876129738L;
    @Id
    private Integer gameId;

    private String yearOfRelease;

    private Double criticsScore;
    private Double usersScore;

    private Double northAmericaSales;
    private Double europeSales;
    private Double japanSales;
    private Double otherSales;
    private Double globalSales;

    private String genreName;
    private Integer genreId;

    private String ratingName;
    private Integer ratingId;

    private String platformName;
    private Integer platformId;
}
