package pl.agh.podgrzybkiemcompany.gamestatistics.dto.game;

import org.springframework.stereotype.Repository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.FavouriteGameListEntry;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.GameCountQueryBuilder;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.UserSpecificListQueryBuilder;

@Repository
public class FavouriteGameListEntryRepositoryImpl
    extends DashboardGameListEntryRepositoryImpl<FavouriteGameListEntry> {

    public FavouriteGameListEntryRepositoryImpl(UserSpecificListQueryBuilder<FavouriteGameListEntry> queryBuilder,
                                                GameCountQueryBuilder countQueryBuilder) {
        super(queryBuilder, countQueryBuilder);
    }
}
