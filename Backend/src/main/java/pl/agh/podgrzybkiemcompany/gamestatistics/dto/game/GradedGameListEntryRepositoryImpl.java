package pl.agh.podgrzybkiemcompany.gamestatistics.dto.game;

import org.springframework.stereotype.Repository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GradedGameListEntry;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.GameCountQueryBuilder;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.UserSpecificListQueryBuilder;

@Repository
public class GradedGameListEntryRepositoryImpl
    extends DashboardGameListEntryRepositoryImpl<GradedGameListEntry> {

    public GradedGameListEntryRepositoryImpl(UserSpecificListQueryBuilder<GradedGameListEntry> queryBuilder,
                                             GameCountQueryBuilder countQueryBuilder) {
        super(queryBuilder, countQueryBuilder);
    }
}
