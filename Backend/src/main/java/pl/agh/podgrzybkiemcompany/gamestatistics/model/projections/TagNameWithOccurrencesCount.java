package pl.agh.podgrzybkiemcompany.gamestatistics.model.projections;

public interface TagNameWithOccurrencesCount {
    String getTagName();
    String getOccurrencesCount();
}
