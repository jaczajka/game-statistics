package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Immutable
@ToString
@Getter
@Setter
public class GradedGameListEntry {

    private Integer titleId;
    private String title;
    private Integer genreId;
    private String genre;
    private Integer clientId;
    private Double givenScore;
    @Id
    private Integer gameId;
    private String platform;
    private String developers;
}
