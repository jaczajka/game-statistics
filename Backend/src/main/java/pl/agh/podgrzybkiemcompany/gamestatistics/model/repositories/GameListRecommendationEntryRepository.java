package pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.dto.game.GameListRecommendationEntryRepositoryExtension;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameListRecommendationEntry;

public interface GameListRecommendationEntryRepository extends JpaRepository<GameListRecommendationEntry, Integer>, GameListRecommendationEntryRepositoryExtension {
}
