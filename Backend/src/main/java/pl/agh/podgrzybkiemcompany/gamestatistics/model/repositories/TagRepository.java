package pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Tag;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.projections.TagNameWithOccurrencesCount;

import java.util.Collection;
import java.util.Optional;

public interface TagRepository extends CrudRepository<Tag, Integer> {
    @Query(value = "SELECT t.tagName.name AS tagName "            +
                        ", COUNT(t.tagName) AS occurrencesCount " +
                   "FROM Tag AS t "                               +
                   "WHERE t.relatedTitle.titleId = :titleId "     +
                   "GROUP BY tagName "                            +
                   "ORDER BY occurrencesCount DESC")
    Collection<TagNameWithOccurrencesCount> findTagOccurrencesCountForTitle(
            @Param("titleId") Integer titleId
    );

    @Query(value = "SELECT t.tagName.name AS tagName "     +
            ", COUNT(t.tagName) AS occurrencesCount "      +
            "FROM Tag AS t "                               +
            "WHERE t.relatedTitle.titleId = :titleId "     +
              "AND t.tagIssuer.name = :username "          +
            "GROUP BY tagName "                            +
            "ORDER BY occurrencesCount DESC")
    Collection<TagNameWithOccurrencesCount> findTagOccurrencesCountForTitleAndClient(
            @Param("titleId") Integer titleId,
            @Param("username") String username
    );

    @Query(value = "SELECT t as tag " +
            "FROM Tag AS t "                               +
            "WHERE t.relatedTitle.titleId = :titleId "     +
            "AND t.tagIssuer.name = :username "          +
            "AND t.tagName.name = :tagName ")
    Optional<Tag> findTagByTitleUserAndName(
            @Param("titleId") Integer titleId,
            @Param("username") String username,
            @Param("tagName") String tagName
    );
}
