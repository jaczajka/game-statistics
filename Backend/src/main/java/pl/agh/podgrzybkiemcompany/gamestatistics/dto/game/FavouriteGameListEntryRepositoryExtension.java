package pl.agh.podgrzybkiemcompany.gamestatistics.dto.game;

import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.FavouriteGameListEntry;

public interface FavouriteGameListEntryRepositoryExtension
    extends DashboardGameListEntryRepositoryExtension<FavouriteGameListEntry> {
}
