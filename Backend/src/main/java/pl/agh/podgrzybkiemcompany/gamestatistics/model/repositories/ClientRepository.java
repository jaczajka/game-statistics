package pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Client;

import java.util.Optional;

public interface ClientRepository extends CrudRepository<Client, Integer> {
    Optional<Client> findByName(String username);
}
