package pl.agh.podgrzybkiemcompany.gamestatistics.queries;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameListEntry;

import javax.persistence.EntityManager;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GameListQueryBuilder extends PaginatedListQueryBuilder<GameListEntry> {

    @Autowired
    public GameListQueryBuilder(EntityManager entityManager, SortingOrderAdapter sortingAdapter) {
        super(entityManager, sortingAdapter, GameListEntry.class, GameListEntry.class);
    }
}
