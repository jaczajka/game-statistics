package pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.dto.game.GameDiagramEntryRepositoryExtension;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameDiagramEntry;

public interface GameDiagramEntryRepository extends JpaRepository<GameDiagramEntry, Integer>, GameDiagramEntryRepositoryExtension {
}
