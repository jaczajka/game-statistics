package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString(exclude = {"clientScores", "favouriteTitles", "preferredGenres", "issuedTags"})
@Entity
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer clientId;

    @NotNull
    @Column(unique = true)
    private String name;
    @JsonIgnore
    private String hash;
    @JsonIgnore
    private String salt;

    @JsonIgnore
    @OneToMany(mappedBy = "scoreIssuer")
    private Set<ClientScore> clientScores = new HashSet<>();
    public void addClientScore(ClientScore clientScore){
        clientScores.add(clientScore);
    }

    @JsonIgnore
    @ManyToMany
    private Set<Title> favouriteTitles = new HashSet<>();
    public void addFavouriteTitle(Title title){
        favouriteTitles.add(title);
    }
    public void removeFavouriteTitle(Title title){
        favouriteTitles.remove(title);
    }

    @JsonIgnore
    @ManyToMany
    private Set<Genre> preferredGenres = new HashSet<>();
    public void addPreferredGenre(Genre genre){
        preferredGenres.add(genre);
    }

    @JsonIgnore
    @OneToMany(mappedBy = "tagIssuer")
    private Set<Tag> issuedTags = new HashSet<>();
    public void addIssuedTag(Tag tag){
        this.issuedTags.add(tag);
    }


    public Client(){}
    public Client(String name, String password) {
        this.name = name;
        this.hash = getPasswordEncoder().encode(password);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
