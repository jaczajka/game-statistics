package pl.agh.podgrzybkiemcompany.gamestatistics.controllers.wrappers;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
public class SimpleDiagramDataRow {
    String yearOfRelease;
    Double value;

    public SimpleDiagramDataRow(String key, Double val) {
        this.yearOfRelease = key;
        this.value = val;
    }
    public SimpleDiagramDataRow(){}

    @Override
    public String toString(){
        return "{\"year\": \"" + this.yearOfRelease + "\", " + "\"value\": " + value + "}";
    }

    public String getYearOfRelease() {
        return yearOfRelease;
    }

    public Double getValue() {
        return value;
    }
}
