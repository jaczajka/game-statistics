package pl.agh.podgrzybkiemcompany.gamestatistics.queries;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public abstract class PaginatedListQueryBuilder<T> extends AbstractGameQueryBuilder<T, T> {

    private final EntityManager entityManager;
    private final SortingOrderAdapter sortingAdapter;
    private final List<Order> sortingOrder = new ArrayList<>();

    private int perPage = 10;
    private int page = 1;

    public PaginatedListQueryBuilder(EntityManager entityManager, SortingOrderAdapter sortingAdapter, Class<T> resultType, Class<T> fromType) {
        super(entityManager, resultType, fromType);
        this.entityManager = entityManager;
        this.sortingAdapter = sortingAdapter;
    }

    public PaginatedListQueryBuilder<T> setPerPage(int maxResults) {
        perPage = maxResults;
        return this;
    }

    public PaginatedListQueryBuilder<T> setPage(int pageNumber) {
        page = pageNumber;
        return this;
    }

    public PaginatedListQueryBuilder<T> orderBy(List<String> orderList) {
        if (orderList != null) {
            List<SortingOrderAdapter.SortColumn> columns = sortingAdapter.getColumns(orderList);
            Sorter sorter = Sorter.createForQuery(queryBuilder);
            columns.forEach((column) ->
                sorter.addSortBy(column.getColumnName(), column.getDirection(), from)
            );
            sortingOrder.addAll(sorter.getOrder());
        }
        return this;
    }

    @Override
    public TypedQuery<T> build() {
        CriteriaQuery<T> dbQuery = query
            .select(from)
            .where(conjunctionOfPredicates())
            .orderBy(sortingOrder)
            .distinct(true);
        TypedQuery<T> typedQuery = entityManager.createQuery(dbQuery)
            .setMaxResults(perPage)
            .setFirstResult(getFirstResultIndex());
        parameters.forEach(typedQuery::setParameter);
        reset();
        return typedQuery;
    }

    private int getFirstResultIndex() {
        return (page - 1) * perPage;
    }

    protected void reset() {
        super.reset();
        sortingOrder.clear();
    }

    protected Predicate conjunctionOfPredicates() {
        Predicate[] predicates = wheres.toArray(new Predicate[] {});
        return queryBuilder.and(predicates);
    }
}
