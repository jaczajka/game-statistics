package pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.dto.game.GradedGameListEntryRepositoryExtension;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GradedGameListEntry;

public interface GradedGameListEntryRepository extends JpaRepository<GradedGameListEntry, Integer>, GradedGameListEntryRepositoryExtension {
}
