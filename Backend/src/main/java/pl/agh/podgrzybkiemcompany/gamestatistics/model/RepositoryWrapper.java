package pl.agh.podgrzybkiemcompany.gamestatistics.model;

import org.springframework.stereotype.Component;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.*;

@Component
public class RepositoryWrapper {

    private final GameListEntryRepository gameListEntryRepository;
    private final GameDiagramEntryRepository gameDiagramEntryRepository;
    private final TagRepository tagRepository;
    private final TitleRepository titleRepository;
    private final GameRepository gameRepository;
    private final GenreRepository genreRepository;
    private final ClientRepository clientRepository;
    private final ClientScoreRepository clientScoreRepository;
    private final TagNameRepository tagNameRepository;
    private final GameListRecommendationEntryRepository gameListRecommendationEntryRepository;
    private final GradedGameListEntryRepository gradedGameListEntryRepository;
    private final FavouriteGameListEntryRepository favouriteGameListEntryRepository;

    public RepositoryWrapper(
            GameListEntryRepository gameListRepository,
            GameDiagramEntryRepository gameDiagramEntryRepository,
            TitleRepository titleRepository,
            GameRepository gameRepository,
            GenreRepository genreRepository,
            ClientRepository clientRepository,
            ClientScoreRepository clientScoreRepository,
            TagRepository tagRepository,
            TagNameRepository tagNameRepository,
            GameListRecommendationEntryRepository gameListRecommendationEntryRepository,
            GradedGameListEntryRepository gradedGameListEntryRepository,
            FavouriteGameListEntryRepository favouriteGameListEntryRepository
    ) {
        this.gameListEntryRepository = gameListRepository;
        this.gameDiagramEntryRepository = gameDiagramEntryRepository;
        this.tagNameRepository = tagNameRepository;
        this.tagRepository = tagRepository;
        this.titleRepository = titleRepository;
        this.gameRepository = gameRepository;
        this.genreRepository = genreRepository;
        this.clientRepository = clientRepository;
        this.clientScoreRepository = clientScoreRepository;
        this.gameListRecommendationEntryRepository = gameListRecommendationEntryRepository;
        this.gradedGameListEntryRepository = gradedGameListEntryRepository;
        this.favouriteGameListEntryRepository = favouriteGameListEntryRepository;
    }

    public GameListEntryRepository getGameListEntryRepository() {
        return gameListEntryRepository;
    }

    public GameDiagramEntryRepository getGameDiagramEntryRepository() {
        return gameDiagramEntryRepository;
    }

    public TagRepository getTagRepository() {
        return tagRepository;
    }

    public TitleRepository getTitleRepository() {
        return titleRepository;
    }

    public GameRepository getGameRepository() {
        return gameRepository;
    }

    public GenreRepository getGenreRepository() {
        return genreRepository;
    }

    public ClientRepository getClientRepository() {
        return clientRepository;
    }

    public ClientScoreRepository getClientScoreRepository() {
        return clientScoreRepository;
    }

    public TagNameRepository getTagNameRepository() {
        return tagNameRepository;
    }

    public GameListRecommendationEntryRepository getGameListRecommendationEntryRepository() {
        return gameListRecommendationEntryRepository;
    }

    public GradedGameListEntryRepository getGradedGameListEntryRepository() {
        return gradedGameListEntryRepository;
    }

    public FavouriteGameListEntryRepository getFavouriteGameListEntryRepository() {
        return favouriteGameListEntryRepository;
    }
}
