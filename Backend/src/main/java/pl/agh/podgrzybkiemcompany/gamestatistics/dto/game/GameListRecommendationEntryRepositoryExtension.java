package pl.agh.podgrzybkiemcompany.gamestatistics.dto.game;

import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameListEntry;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameListRecommendationEntry;
import pl.agh.podgrzybkiemcompany.gamestatistics.utils.Page;

import java.util.List;

public interface GameListRecommendationEntryRepositoryExtension {
    Page<GameListRecommendationEntry> getPaginatedList(
            String username,
            String searchPhrase,
            List<String> genres,
            Integer minUsersScore,
            Integer minCriticsScore,
            Integer maxUsersScore,
            Integer maxCriticsScore,
            List<String> developers,
            Integer minScoresCount,
            Integer page,
            Integer perPage,
            List<String> sortBy
    );
}
