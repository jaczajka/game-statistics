package pl.agh.podgrzybkiemcompany.gamestatistics.dto.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameListEntry;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.GameCountQueryBuilder;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.GameListQueryBuilder;
import pl.agh.podgrzybkiemcompany.gamestatistics.utils.Page;

import java.util.List;

@Repository
public class GameListEntryRepositoryImpl implements ListRepositoryExtension {

    private final GameListQueryBuilder listQueryBuilder;
    private final GameCountQueryBuilder countQueryBuilder;

    @Autowired
    public GameListEntryRepositoryImpl(GameListQueryBuilder queryBuilder, GameCountQueryBuilder countQueryBuilder) {
        this.listQueryBuilder = queryBuilder;
        this.countQueryBuilder = countQueryBuilder;
    }

    @Override
    public Page<GameListEntry> getPaginatedList(
            String searchPhrase,
            List<String> genres,
            Integer minUsersScore,
            Integer minCriticsScore,
            Integer maxUsersScore,
            Integer maxCriticsScore,
            List<String> developers,
            Integer minScoresCount,
            Integer page,
            Integer perPage,
            List<String> orderBy
    ) {
        List<GameListEntry> items = listQueryBuilder
                .orderBy(orderBy)
                .setPerPage(perPage)
                .setPage(page)
                .filterByTitlePhrase(searchPhrase)
                .filterByDevelopers(developers)
                .filterByGenres(genres)
                .filterByUsersScore(minUsersScore, maxUsersScore)
                .filterByCriticsScore(minCriticsScore, maxCriticsScore)
                .filterByMinScoresCount(minScoresCount)
                .build()
                .getResultList();
        Long totalCount = countQueryBuilder
                .filterByTitlePhrase(searchPhrase)
                .filterByDevelopers(developers)
                .filterByGenres(genres)
                .filterByUsersScore(minUsersScore, maxUsersScore)
                .filterByCriticsScore(minCriticsScore, maxCriticsScore)
                .filterByMinScoresCount(minScoresCount)
                .build()
                .getSingleResult();
        return new Page<>(items, totalCount, page);
    }
}
