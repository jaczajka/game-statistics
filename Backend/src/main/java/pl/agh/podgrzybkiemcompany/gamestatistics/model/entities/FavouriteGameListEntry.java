package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Immutable
@ToString
@Getter
@Setter
public class FavouriteGameListEntry {

    @Id
    private Integer titleId;
    private String title;
    private Integer genreId;
    private String genre;
    private Integer averageUsersCount;
    private Integer averageCriticsCount;
    private Integer averageUsersScore;
    private Integer averageCriticsScore;
    private Integer clientId;
    private String developers;
}
