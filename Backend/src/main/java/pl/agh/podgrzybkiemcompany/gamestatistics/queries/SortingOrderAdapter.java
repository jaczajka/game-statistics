package pl.agh.podgrzybkiemcompany.gamestatistics.queries;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SortingOrderAdapter {

    public List<SortColumn> getColumns(List<String> orderList) {
        List<SortColumn> columns = new ArrayList<>();
        for (int i = 0; i < orderList.size(); i += 2) {
            columns.add(new SortColumn(orderList.get(i), orderList.get(i + 1)));
        }
        return columns;
    }

    public static class SortColumn {
        private String columnName;
        private String direction;

        public SortColumn(String columnName, String direction) {
            this.columnName = columnName;
            this.direction = direction;
        }

        public String getColumnName() {
            return columnName;
        }

        public String getDirection() {
            return direction;
        }
    }
}
