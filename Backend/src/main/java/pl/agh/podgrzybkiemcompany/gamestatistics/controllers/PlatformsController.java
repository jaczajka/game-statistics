package pl.agh.podgrzybkiemcompany.gamestatistics.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Platform;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Title;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.*;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@RestController
public class PlatformsController {

    private final PlatformRepository platformRepository;

    @Autowired
    public PlatformsController(
            PlatformRepository platformRepository
    ) {
        this.platformRepository = platformRepository;
    }


    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("games/platforms")
    public Collection<Platform> platformListAction() {
        List<Platform> result = new LinkedList<>();
        platformRepository
                .findAll()
                .forEach(result::add);

        return result;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("games/platforms/names")
    public Collection<String> platformNamesListAction() {
        List<String> result = new LinkedList<>();
        platformRepository
                .findAll()
                .forEach(platform -> result.add(platform.getName()));

        return result;
    }
}
