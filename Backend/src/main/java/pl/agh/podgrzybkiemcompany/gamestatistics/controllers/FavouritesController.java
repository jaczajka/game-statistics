package pl.agh.podgrzybkiemcompany.gamestatistics.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.RepositoryWrapper;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Client;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.FavouriteGameListEntry;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Title;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.TitleRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.utils.ClientRepositoryManager;
import pl.agh.podgrzybkiemcompany.gamestatistics.utils.Page;

import java.util.*;

@RestController
public class FavouritesController {
    private final ClientRepositoryManager clientRepository;
    private final TitleRepository titleRepository;

    private final RepositoryWrapper repositoryWrapper;

    @Autowired
    public FavouritesController(ClientRepositoryManager clientRepository,
                                TitleRepository titleRepository,
                                RepositoryWrapper repositoryWrapper) {
        this.clientRepository = clientRepository;
        this.titleRepository = titleRepository;
        this.repositoryWrapper = repositoryWrapper;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("games/favourites/fond-of-title")
    public Integer fondOfTitle(
            @RequestParam(value = "titleId") Integer titleId
    ){
        Title title = titleRepository.findById(titleId).get();
        return title.getFavouriteOf().size();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/games/favourites/user-favourites-count")
    public Integer favouritesOfUserCount(
            @RequestParam(value = "username") String username
    )
    {
        Collection<Title> title
            = titleRepository.findByFavouritesOfClient(username);
        return title.size();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("games/favourites/ratio")
    public double favouritesRatio(@RequestParam(value = "titleId") Integer titleId){
        Title title = titleRepository.findById(titleId).get();
        int allWhoLikedTheGame = title.getFavouriteOf().size();
        long allUsers = clientRepository.getClientRepository().count();
        if (allUsers == 0) return 0.0;
        return (double) allWhoLikedTheGame / allUsers;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("games/favourites")
    public boolean isUserFavourite(
            @RequestParam(value = "titleId") Integer titleId,
            @RequestParam(value = "username") String username
    ){
        if (clientRepository.findByUsername(username).isEmpty())
            return false;
        Client client = clientRepository.findByUsername(username).get();
        Title title = titleRepository.findById(titleId).get();
        Set<Title> userFavourites = client.getFavouriteTitles();

        return userFavourites.contains(title);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("games/favourites")
    public void addToFavourites(
            @RequestParam(value = "titleId") Integer titleId,
            @RequestParam(value = "username") String username
    ){
        Client client = clientRepository.findByUsername(username).get();
        Title title = titleRepository.findById(titleId).get();
        Set<Title> userFavourites = client.getFavouriteTitles();
        if (userFavourites.contains(title)) {
            client.removeFavouriteTitle(title);
            clientRepository.getClientRepository().save(client);
        } else {
            client.addFavouriteTitle(title);
            clientRepository.getClientRepository().save(client);
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/games/favourites/{username}")
    public Page<FavouriteGameListEntry> favouriteGameListAction(
            @PathVariable String username,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "perPage", defaultValue = "10") Integer perPage
    ) {
        return repositoryWrapper.getFavouriteGameListEntryRepository().getPaginatedList(
                username,
                page,
                perPage
        );
    }

}
