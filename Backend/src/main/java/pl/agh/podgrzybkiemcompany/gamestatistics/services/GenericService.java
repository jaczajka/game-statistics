package pl.agh.podgrzybkiemcompany.gamestatistics.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

public class GenericService<T, D> {
    @Autowired
    private CrudRepository<T, D> repository;

    public Optional<T> findById(D id){
        return repository.findById(id);
    }

    public Iterable<T> findAll(){
        return repository.findAll();
    }

    public boolean existsById(D id){
        return repository.existsById(id);
    }

    public long count(){
        return repository.count();
    }



    public void delete(T entity){
        repository.delete(entity);
    }

    public void deleteAll(){
        repository.deleteAll();
    }

    public void deleteAll(Iterable<? extends T> entities){
        repository.deleteAll(entities);
    }

    public void deleteById(D id){
        repository.deleteById(id);
    }



    public void save(T t){
        repository.save(t);
    }

    public void saveAll(Iterable<T> entities){
        repository.deleteAll(entities);
    }


}
