package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString(exclude = {"associatedTags"})
@Entity
public class TagName {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer tagId;

    @NotNull
    @Column(unique = true)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "tagName")
    private Set<Tag> associatedTags = new HashSet<>();
    public void addAssociatedTags(Tag tag){
        this.associatedTags.add(tag);
    }


    public TagName() {
    }
    public TagName(String name) {
        this.name = name;
    }
}
