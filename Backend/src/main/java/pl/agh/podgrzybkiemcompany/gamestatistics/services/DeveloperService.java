package pl.agh.podgrzybkiemcompany.gamestatistics.services;

import org.springframework.stereotype.Service;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Developer;

@Service
public class DeveloperService extends GenericService<Developer, Integer> {
}
