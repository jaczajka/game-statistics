package pl.agh.podgrzybkiemcompany.gamestatistics.services;

import org.springframework.stereotype.Service;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Title;

@Service
public class TitleService extends GenericService<Title, Integer>{
}
