package pl.agh.podgrzybkiemcompany.gamestatistics.importer;

public enum Column {
    NAME,
    PLATFORM,
    YEAR_OF_RELEASE,
    GENRE,
    PUBLISHER,
    NA_SALES,
    EU_SALES,
    JP_SALES,
    OTHER_SALES,
    GLOBAL_SALES,
    CRITIC_SCORE,
    CRITIC_COUNT,
    USER_SCORE,
    USER_COUNT,
    DEVELOPER,
    RATING
}
