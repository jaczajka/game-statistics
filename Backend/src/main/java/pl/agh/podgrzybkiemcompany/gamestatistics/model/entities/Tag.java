package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString()
@Entity
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer tagId;

    @NotNull
    @ManyToOne
    private TagName tagName;

    @NotNull
    @ManyToOne
    private Title relatedTitle;

    @NotNull
    @ManyToOne
    private Client tagIssuer;

    public Tag(TagName name, Title title, Client tagIssuer) {
        this.tagName = name;
        this.relatedTitle = title;
        this.tagIssuer = tagIssuer;
    }
    public Tag(){

    }
}
