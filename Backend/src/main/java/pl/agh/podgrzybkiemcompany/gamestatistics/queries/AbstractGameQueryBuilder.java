package pl.agh.podgrzybkiemcompany.gamestatistics.queries;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractGameQueryBuilder<T, X> {

    public static final String PHRASE_PARAM = "phrase";
    public static final String MIN_CRITICS_SCORE_PARAM = "minCriticsScore";
    public static final String MIN_USERS_SCORE_PARAM = "minUsersScore";
    public static final String MAX_CRITICS_SCORE_PARAM = "maxCriticsScore";
    public static final String MAX_USERS_SCORE_PARAM = "maxUsersScore";
    public static final String MIN_SCORES_COUNT_PARAM = "minScoresCount";
    public static final String DEVELOPER_PARAM = "developer";
    public static final String GENRES_PARAM = "genreIds";

    protected final CriteriaQuery<T> query;
    protected final CriteriaBuilder queryBuilder;
    protected final Root<X> from;

    protected final List<Predicate> wheres = new ArrayList<>();
    protected final Map<String, Object> parameters = new HashMap<>();

    public AbstractGameQueryBuilder(EntityManager entityManager, Class<T> resultType, Class<X> fromType) {
        queryBuilder = entityManager.getCriteriaBuilder();
        query = queryBuilder.createQuery(resultType);
        from = query.from(fromType);
    }

    public AbstractGameQueryBuilder<T, X> filterByTitlePhrase(String phrase) {
        if (phrase != null) {
            wheres.add(queryBuilder.like(
                    queryBuilder.lower(from.get("title")),
                    queryBuilder.lower(queryBuilder.parameter(String.class, PHRASE_PARAM)))
            );
            this.parameters.put(PHRASE_PARAM, "%" + phrase + "%");
        }
        return this;
    }


    public AbstractGameQueryBuilder<T, X> filterByDevelopers(List<String> developers) {
        if (developers != null) {
            Predicate[] orList = new Predicate[developers.size()];
            for (int i = 0; i < developers.size(); ++i) {
                orList[i] = queryBuilder.like(
                        queryBuilder.lower(from.get("developers")),
                        queryBuilder.lower(queryBuilder.parameter(String.class, DEVELOPER_PARAM + i))
                );
                this.parameters.put(DEVELOPER_PARAM + i, "%" + developers.get(i) + "%");
            }
            wheres.add(queryBuilder.or(orList));
        }
        return this;
    }

    public AbstractGameQueryBuilder<T, X> filterByGenres(List<String> genres) {
        if (genres != null) {
            Predicate[] orList = new Predicate[genres.size()];
            for (int i = 0; i < genres.size(); ++i) {
                orList[i] = queryBuilder.like(
                        queryBuilder.lower(from.get("genre")),
                        queryBuilder.lower(queryBuilder.parameter(String.class, GENRES_PARAM + i))
                );
                this.parameters.put(GENRES_PARAM + i, "%" + genres.get(i) + "%");
            }
            wheres.add(queryBuilder.or(orList));
        }
        return this;
    }

    public AbstractGameQueryBuilder<T, X> filterByUsersScore(Integer minScore, Integer maxScore) {
        if (minScore != null) {
            wheres.add(queryBuilder.gt(
                    from.get("averageUsersScore"),
                    queryBuilder.parameter(Integer.class, MIN_USERS_SCORE_PARAM))
            );
            this.parameters.put(MIN_USERS_SCORE_PARAM, minScore);
        }
        if (maxScore != null) {
            wheres.add(queryBuilder.lt(
                    from.get("averageUsersScore"),
                    queryBuilder.parameter(Integer.class, MAX_USERS_SCORE_PARAM))
            );
            this.parameters.put(MAX_USERS_SCORE_PARAM, maxScore);
        }
        return this;
    }

    public AbstractGameQueryBuilder<T, X> filterByCriticsScore(Integer minScore, Integer maxScore) {
        if (minScore != null) {
            wheres.add(queryBuilder.gt(
                    from.get("averageCriticsScore"),
                    queryBuilder.parameter(Integer.class, MIN_CRITICS_SCORE_PARAM))
            );
            this.parameters.put(MIN_CRITICS_SCORE_PARAM, minScore * 10);
        }
        if (maxScore != null) {
            wheres.add(queryBuilder.lt(
                    from.get("averageCriticsScore"),
                    queryBuilder.parameter(Integer.class, MAX_CRITICS_SCORE_PARAM))
            );
            this.parameters.put(MAX_CRITICS_SCORE_PARAM, maxScore * 10);
        }
        return this;
    }

    public AbstractGameQueryBuilder<T, X> filterByMinScoresCount(Integer minScoresCount) {
        if (minScoresCount != null) {
            wheres.add(queryBuilder.gt(
                    from.get("averageUsersCount"),
                    queryBuilder.parameter(Integer.class, MIN_SCORES_COUNT_PARAM))
            );
            wheres.add(queryBuilder.gt(
                    from.get("averageCriticsCount"),
                    queryBuilder.parameter(Integer.class, MIN_SCORES_COUNT_PARAM))
            );
            this.parameters.put(MIN_SCORES_COUNT_PARAM, minScoresCount);
        }
        return this;
    }

    abstract public TypedQuery<T> build();

    protected void reset() {
        wheres.clear();
        parameters.clear();
    }
}
