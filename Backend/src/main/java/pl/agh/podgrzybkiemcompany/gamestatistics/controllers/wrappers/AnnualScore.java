package pl.agh.podgrzybkiemcompany.gamestatistics.controllers.wrappers;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AnnualScore {
    private String year;
    private Double userScore;
    private Double criticScore;

    public AnnualScore(String year, Double userScore, Double criticScore) {
        this.year = year;
        this.userScore = userScore;
        this.criticScore = criticScore;
    }
}
