package pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Genre;
@Repository
public interface GenreRepository extends CrudRepository<Genre, Integer> {
}
