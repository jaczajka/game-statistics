package pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Platform;
@Repository
public interface PlatformRepository extends CrudRepository<Platform, Integer> {
}
