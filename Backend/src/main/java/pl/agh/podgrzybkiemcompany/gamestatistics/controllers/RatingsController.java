package pl.agh.podgrzybkiemcompany.gamestatistics.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Platform;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Rating;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.PlatformRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.RatingRepository;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@RestController
public class RatingsController {
    private final RatingRepository ratingRepository;
    List<String> nonPermittedRatings = List.of("Not Known", "");

    @Autowired
    public RatingsController(
            RatingRepository ratingRepository
    ) {
        this.ratingRepository = ratingRepository;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("games/ratings")
    public Collection<Rating> platformListAction() {
        List<Rating> result = new LinkedList<>();
        ratingRepository
                .findAll()
                .forEach(rating -> {
                    if (! nonPermittedRatings.contains(rating.getName())) {
                        result.add(rating);
                    }
                });

        return result;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("games/ratings/names")
    public Collection<String> ratingNamesListAction() {

        List<String> result = new LinkedList<>();
        ratingRepository
                .findAll()
                .forEach(rating -> {
                    if (! nonPermittedRatings.contains(rating.getName())) {
                        result.add(rating.getName());
                    }
                });


        return result;
    }
}
