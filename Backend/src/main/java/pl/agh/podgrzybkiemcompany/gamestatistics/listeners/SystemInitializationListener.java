package pl.agh.podgrzybkiemcompany.gamestatistics.listeners;

import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import pl.agh.podgrzybkiemcompany.gamestatistics.importer.Importer;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.TitleRepository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Component
public class SystemInitializationListener {


    private final String[] modelAdjustmentScripts = {
            "sql_scripts/game_list_entry_view.sql",
            "sql_scripts/game_diagram_entry_view.sql",
            "sql_scripts/unique_constraints.sql",
			"sql_scripts/game_list_entry_recommendations_view.sql"
    };
    private EntityManager entityManager;
    private ResourceLoader resourceLoader;
    private TitleRepository titleRepository;
    private Importer importer;

    public SystemInitializationListener(
            EntityManager entityManager,
            ResourceLoader resourceLoader,
            TitleRepository titleRepository,
            Importer importer
    ) {
        this.titleRepository = titleRepository;
        this.entityManager = entityManager;
        this.resourceLoader = resourceLoader;
        this.importer = importer;
    }

    @Transactional
    @EventListener(ApplicationStartedEvent.class)
    public void onApplicationEvent(ApplicationStartedEvent event) {
        System.out.println(entityManager);
        try {
            for (String script : modelAdjustmentScripts) {
                System.out.println("Executing initialization script " + script);
                entityManager.createNativeQuery(getScript(script)).executeUpdate();
            }
            loadData();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private String getScript(String path) throws IOException {
        Resource resource = resourceLoader.getResource("classpath:" + path);
        InputStream inputStream = resource.getInputStream();
        Path tempFile = Files.createTempDirectory("").resolve(UUID.randomUUID().toString() + ".tmp");
        Files.copy(inputStream, tempFile, StandardCopyOption.REPLACE_EXISTING);
        return new String(Files.readAllBytes(tempFile));
    }

    private void loadData() throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        if (titleRepository.count() == 0) {
            System.out.println("Importing data");
            importer.importData();
        }
    }
}
