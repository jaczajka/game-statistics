package pl.agh.podgrzybkiemcompany.gamestatistics.utils;

public class AlreadyRegisteredException extends Exception {
    public AlreadyRegisteredException(String message) {
        super(message);
    }
}
