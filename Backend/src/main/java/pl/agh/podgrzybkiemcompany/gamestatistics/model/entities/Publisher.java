package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Getter
@Setter
@ToString(exclude = {"titles"})
@Entity
public class Publisher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer publisherId;
    @NotNull private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "publisher")
    private Set<Title> titles = new HashSet<>();


    public Publisher(String name) {
        this.name = name;
    }
    public Publisher(){
    }

}
