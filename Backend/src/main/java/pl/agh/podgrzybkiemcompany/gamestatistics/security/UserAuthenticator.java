package pl.agh.podgrzybkiemcompany.gamestatistics.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserAuthenticator implements AuthenticationProvider {

    @Autowired
    private UserAuthService userAuthService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        final var token = (UsernamePasswordAuthenticationToken) authentication;

        final var username = (String) token.getPrincipal();
        UserDetails user = userAuthService.loadUserByUsername(username);

        final var password = (String) token.getCredentials();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        if (!encoder.matches(password, user.getPassword()))
            throw new BadCredentialsException("Bad credentials for user " + username);
        return authentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return false;
    }
}
