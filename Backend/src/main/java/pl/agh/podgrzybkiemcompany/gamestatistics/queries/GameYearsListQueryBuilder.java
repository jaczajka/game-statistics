package pl.agh.podgrzybkiemcompany.gamestatistics.queries;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameDiagramEntry;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameListEntry;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;


@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GameYearsListQueryBuilder extends AbstractGameQueryBuilder<String, GameDiagramEntry> {
    private final EntityManager entityManager;

    @Autowired
    public GameYearsListQueryBuilder(EntityManager entityManager) {
        super(entityManager, String.class, GameDiagramEntry.class);
        this.entityManager = entityManager;
    }

    public TypedQuery<String> build() {
        CriteriaQuery<String> dbQuery = query
                .select(from.get("yearOfRelease"))
                .distinct(true)
                .orderBy(queryBuilder.asc(from.get("yearOfRelease")));
        TypedQuery<String> typedQuery = entityManager.createQuery(dbQuery);
        parameters.forEach(typedQuery::setParameter);
        reset();
        return typedQuery;
    }
}
