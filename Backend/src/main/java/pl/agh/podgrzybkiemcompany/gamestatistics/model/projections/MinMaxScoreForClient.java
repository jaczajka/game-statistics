package pl.agh.podgrzybkiemcompany.gamestatistics.model.projections;

public class MinMaxScoreForClient {

    private final Double minScore;
    private final Double maxScore;

    public MinMaxScoreForClient(Double minScore, Double maxScore) {
        this.minScore = minScore;
        this.maxScore = maxScore;
    }

    public Double getMinScore() {
        return minScore;
    }

    public Double getMaxScore() {
        return maxScore;
    }
}
