package pl.agh.podgrzybkiemcompany.gamestatistics.queries;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.agh.podgrzybkiemcompany.gamestatistics.controllers.wrappers.SimpleDiagramDataRow;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameDiagramEntry;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

import java.util.LinkedList;
import java.util.List;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GameDiagramQueryBuilder extends AbstractGameQueryBuilder<SimpleDiagramDataRow, GameDiagramEntry> {

    public static final String PLATFORM_PARAM = "platformName";
    public static final String GENRE_PARAM =  "genreName";
    public static final String RATING_PARAM = "ratingName";

    private String region = "global";

    public enum AggregateOperation{
        AVG,
        SUM
    }
    private AggregateOperation aggregateOperation;
    private String aggregatedColumn;

    private final EntityManager entityManager;
    private List<String> groupings = new LinkedList<>();

    public GameDiagramQueryBuilder filterByPlatformName(String name) {
        if (name != null) {
            wheres.add(queryBuilder.like(
                    queryBuilder.lower(from.get("platformName")),
                    queryBuilder.lower(queryBuilder.parameter(String.class, PLATFORM_PARAM)))
            );
            this.parameters.put(PLATFORM_PARAM, "%" + name + "%");
        }
        return this;
    }

    public GameDiagramQueryBuilder filterByGenreName(String name) {
        if (name != null) {
            wheres.add(queryBuilder.like(
                    queryBuilder.lower(from.get("genreName")),
                    queryBuilder.lower(queryBuilder.parameter(String.class, GENRE_PARAM)))
            );
            this.parameters.put(GENRE_PARAM, "%" + name + "%");
        }
        return this;
    }

    public GameDiagramQueryBuilder filterByRatingName(String name) {
        if (name != null) {
            wheres.add(queryBuilder.like(
                    queryBuilder.lower(from.get("ratingName")),
                    queryBuilder.lower(queryBuilder.parameter(String.class, RATING_PARAM)))
            );
            this.parameters.put(RATING_PARAM, "%" + name + "%");
        }
        return this;
    }

    public void groupBy(List<String> groupings){
        this.groupings = groupings;
    }

   /* public void setRegion(String region){
        if (! List.of("europe", "global", "japan", "northAmerica", "other").contains(region)){
            //TODO something to signal error
            try {
                throw new Exception("ERROR, invalid region name");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.region = region;
    }*/

    public void setAggregatedColumn(String aggregatedColumn){
        this.aggregatedColumn = aggregatedColumn;
    }
    public void setAggregateOperation(AggregateOperation aggregateOperation){
        this.aggregateOperation = aggregateOperation;
    }

    @Autowired
    public GameDiagramQueryBuilder(EntityManager entityManager) {
        super(entityManager, SimpleDiagramDataRow.class, GameDiagramEntry.class);
        this.entityManager = entityManager;
    }

    @Override
    public TypedQuery<SimpleDiagramDataRow> build() {
        Expression aggregation = null;
        if (this.aggregateOperation == AggregateOperation.AVG){
            aggregation = queryBuilder.avg(from.get(aggregatedColumn));
        }
        else if (this.aggregateOperation == AggregateOperation.SUM) {
            aggregation = queryBuilder.sum(from.get(aggregatedColumn));
        }

        CriteriaQuery<SimpleDiagramDataRow> dbQuery = query
                .multiselect(from.get("yearOfRelease"), aggregation )
                .where(queryBuilder.and(wheres.toArray(new Predicate[]{})))
                .groupBy(from.get("yearOfRelease"));
        /*if (groupings != null) {
            groupings.forEach(grouping -> query.groupBy(from.get(grouping)));
        }*/

        TypedQuery<SimpleDiagramDataRow> typedQuery = entityManager.createQuery(dbQuery);
        parameters.forEach(typedQuery::setParameter);

        reset();
        return typedQuery;
    }


    protected void reset() {
        super.reset();
    }
}
