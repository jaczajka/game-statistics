package pl.agh.podgrzybkiemcompany.gamestatistics.dto.game;

import pl.agh.podgrzybkiemcompany.gamestatistics.controllers.wrappers.SimpleDiagramDataRow;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.GameDiagramQueryBuilder;

import java.util.List;

public interface GameDiagramEntryRepositoryExtension {


    List<SimpleDiagramDataRow> getDiagramData(
            String aggregatedColumn,
            GameDiagramQueryBuilder.AggregateOperation aggregateOperation,
            String platformName,
            String genreName,
            String ratingName,
            List<String> groupings
    );
    List<String> getYearsList();
}
