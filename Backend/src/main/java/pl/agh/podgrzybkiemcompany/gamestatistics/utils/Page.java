package pl.agh.podgrzybkiemcompany.gamestatistics.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Page <T> implements Serializable {
    private List<T> items;
    private long totalCount;
    private int page;

    public Page(List<T> items, long totalCount, int page) {
        this.items = items;
        this.totalCount = totalCount;
        this.page = page;
    }
}
