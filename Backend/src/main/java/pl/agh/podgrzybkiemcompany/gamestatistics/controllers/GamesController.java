package pl.agh.podgrzybkiemcompany.gamestatistics.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import pl.agh.podgrzybkiemcompany.gamestatistics.controllers.wrappers.AnnualScore;
import pl.agh.podgrzybkiemcompany.gamestatistics.controllers.wrappers.SimpleDiagramDataRow;
import pl.agh.podgrzybkiemcompany.gamestatistics.controllers.wrappers.TitleScore;
import pl.agh.podgrzybkiemcompany.gamestatistics.controllers.exceptions.*;
import pl.agh.podgrzybkiemcompany.gamestatistics.importer.Column;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.RepositoryWrapper;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Client;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameListEntry;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameListRecommendationEntry;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Title;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.GameDiagramQueryBuilder;
import pl.agh.podgrzybkiemcompany.gamestatistics.security.AuthenticationRequest;
import pl.agh.podgrzybkiemcompany.gamestatistics.security.AuthenticationResponse;
import pl.agh.podgrzybkiemcompany.gamestatistics.security.UserAuthService;
import pl.agh.podgrzybkiemcompany.gamestatistics.security.UserData;
import pl.agh.podgrzybkiemcompany.gamestatistics.utils.ClientRepositoryManager;
import pl.agh.podgrzybkiemcompany.gamestatistics.utils.JwtUtils;
import pl.agh.podgrzybkiemcompany.gamestatistics.utils.Page;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class GamesController {

    @Autowired
    private ClientRepositoryManager clientRepositoryManager;
    @Autowired
    private AuthenticationProvider authenticationProvider;
    @Autowired
    private UserAuthService userDetailsService;

    private final RepositoryWrapper repositoryWrapper;

    @Autowired
    public GamesController(RepositoryWrapper repositoryWrapper) {
        this.repositoryWrapper = repositoryWrapper;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/")
    public void homeAction(HttpServletResponse response) throws IOException {
        response.sendRedirect("/games");
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticateAndCreateToken(
            @RequestBody AuthenticationRequest request
    )
        throws Exception
    {
        final String username = request.getUsername();
        final String password = request.getPassword();
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            authenticationProvider.authenticate(token);
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        return createResponseWithToken(userDetails);
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerAndCreateAuthenticationToken(
            @RequestBody AuthenticationRequest request
    )
            throws Exception
    {
        final String username = request.getUsername();
        final String password = request.getPassword();

        Client client = new Client(username, password);
        clientRepositoryManager.register(client);

        UserDetails userDetails = new UserData(client);
        return createResponseWithToken(userDetails);
    }

    public ResponseEntity<?> createResponseWithToken(UserDetails details) {
        final String jwt = JwtUtils.generateToken(details);
        final AuthenticationResponse response = new AuthenticationResponse(jwt);
        return ResponseEntity.ok(response);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/games")
    public Page<GameListEntry> gameListAction(
            @RequestParam(value = "phrase", required = false) String searchPhrase,
            @RequestParam(value = "genre", required = false) List<String> genres,
            @RequestParam(value = "minUsersScore", required = false) Integer minUsersScore,
            @RequestParam(value = "minCriticsScore", required = false) Integer minCriticsScore,
            @RequestParam(value = "maxUsersScore", required = false) Integer maxUsersScore,
            @RequestParam(value = "maxCriticsScore", required = false) Integer maxCriticsScore,
            @RequestParam(value = "developers", required = false) List<String> developers,
            @RequestParam(value = "minScoresCount", required = false) Integer minScoresCount,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "perPage", defaultValue = "10") Integer perPage,
            @RequestParam(value = "sortBy", defaultValue = "title,asc") List<String> sortBy
    ) {
        return repositoryWrapper.getGameListEntryRepository().getPaginatedList(
                searchPhrase,
                genres,
                minUsersScore,
                minCriticsScore,
                maxUsersScore,
                maxCriticsScore,
                developers,
                minScoresCount,
                page,
                perPage,
                sortBy
        );
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/games/recommendations/{username}")
    public Page<GameListRecommendationEntry> gameRecommendationsListAction(
            @PathVariable String username,
            @RequestParam(value = "phrase", required = false) String searchPhrase,
            @RequestParam(value = "genre", required = false) List<String> genres,
            @RequestParam(value = "minUsersScore", required = false) Integer minUsersScore,
            @RequestParam(value = "minCriticsScore", required = false) Integer minCriticsScore,
            @RequestParam(value = "maxUsersScore", required = false) Integer maxUsersScore,
            @RequestParam(value = "maxCriticsScore", required = false) Integer maxCriticsScore,
            @RequestParam(value = "developers", required = false) List<String> developers,
            @RequestParam(value = "minScoresCount", required = false) Integer minScoresCount,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "perPage", defaultValue = "10") Integer perPage,
            @RequestParam(value = "sortBy", required = false) List<String> sortBy
    ) {
        return repositoryWrapper.getGameListRecommendationEntryRepository().getPaginatedList(
                username,
                searchPhrase,
                genres,
                minUsersScore,
                minCriticsScore,
                maxUsersScore,
                maxCriticsScore,
                developers,
                minScoresCount,
                page,
                perPage,
                sortBy
        );
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/games/{titleId}")
    public Title gameDetails(@PathVariable Integer titleId) {
        return repositoryWrapper.getTitleRepository().findById(titleId).get();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/games/annualGrades")
    public Collection<AnnualScore> annualGradesAction(
            @RequestParam(value = "rating", required = false) String ratingName,
            @RequestParam(value = "genre", required = false) String genreName,
            @RequestParam(value = "groupBy", required = false) List<String> groupings
    ) {
        List<String> years = this.yearsListAction();

        repositoryWrapper.getGameDiagramEntryRepository()
                .getDiagramData("usersScore",
                        GameDiagramQueryBuilder.AggregateOperation.AVG,
                        null,
                        genreName,
                        ratingName,
                        groupings
                )
                .stream()
                .filter(row ->
                        !row.getYearOfRelease()
                                .equals("N/A")
                                && row.getValue() != null
                )
                .forEach(System.out::println);

        Map<String, Double> userScores = repositoryWrapper.getGameDiagramEntryRepository()
                .getDiagramData("usersScore",
                        GameDiagramQueryBuilder.AggregateOperation.AVG,
                        null,
                        genreName,
                        ratingName,
                        groupings
                )
                .stream()
                .filter(row ->
                        !row.getYearOfRelease()
                                .equals("N/A")
                                && row.getValue() != null
                )
                .sorted(Comparator.comparing(SimpleDiagramDataRow::getYearOfRelease))

                .collect(Collectors.toMap(SimpleDiagramDataRow::getYearOfRelease, SimpleDiagramDataRow::getValue));

        Map<String, Double> criticScores = repositoryWrapper.getGameDiagramEntryRepository()
                .getDiagramData("criticsScore",
                        GameDiagramQueryBuilder.AggregateOperation.AVG,
                        null,
                        genreName,
                        ratingName,
                        groupings
                )
                .stream()
                .filter(row ->
                        !row.getYearOfRelease()
                                .equals("N/A")
                                && row.getValue() != null
                )
                .sorted(Comparator.comparing(SimpleDiagramDataRow::getYearOfRelease))
                .collect(Collectors.toMap(SimpleDiagramDataRow::getYearOfRelease, SimpleDiagramDataRow::getValue));

        return years.stream().map(year ->
                new AnnualScore(
                        year,
                        userScores.get(year),
                        criticScores.get(year))
        ).collect(Collectors.toList());
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/games/diagrams")
    public Collection<SimpleDiagramDataRow> simpleDiagramAction(
            @RequestParam(value = "region", required = false, defaultValue = "global") String region,
            @RequestParam(value = "platform", required = false) String platformName,
            @RequestParam(value = "genre", required = false) String genreName,
            @RequestParam(value = "rating", required = false) String ratingName,
            @RequestParam(value = "groupBy", required = false) List<String> groupings
    ) {
        //TODO sprawdzic czy ten groupBy sie przyda (na razie jest nie uzywany)


        System.out.println(region);
        System.out.println(platformName);
        System.out.println(genreName);
        System.out.println(ratingName);
        System.out.println(groupings);

        List<SimpleDiagramDataRow> a = repositoryWrapper.getGameDiagramEntryRepository()
                .getDiagramData(region + "Sales",
                        GameDiagramQueryBuilder.AggregateOperation.SUM,
                        platformName,
                        genreName,
                        ratingName,
                        groupings
                )
                .stream()
                .filter(row ->
                        !row.getYearOfRelease()
                                .equals("N/A"))
                .sorted(Comparator.comparing(SimpleDiagramDataRow::getYearOfRelease))
                .collect(Collectors.toList());
        System.out.println(a);
        return a;
    }
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/games/diagrams/similarTitles/{similarColumn}/{titleId}")
    public Collection<TitleScore> similarTitlesAction(
            @PathVariable String similarColumn,
            @PathVariable Integer titleId
    ) throws InvalidColumnException {
        if (! List.of("genre", "publisher").contains(similarColumn)){
            throw new InvalidColumnException();
        }

        Optional<Title> titleOptional = repositoryWrapper.getTitleRepository()
                .findById(titleId);

        if (titleOptional.isPresent()) {
            List<Title> similarColumnValueTitles = new LinkedList<>();
            if (similarColumn.equals("genre")){
                similarColumnValueTitles = List.copyOf(titleOptional.get().getGenre().getTitles());
            }
            else if (similarColumn.equals("publisher")){
                similarColumnValueTitles = List.copyOf(titleOptional.get().getPublisher().getTitles());
            }

            List<TitleScore> titleScores = similarColumnValueTitles.stream()
                    .filter(title -> !title.getTitleId().equals(titleId))
                    .filter(title -> TitleScore.getTitlesAverageScore(title, Column.CRITIC_SCORE) != null)
                    .filter(title -> TitleScore.getTitlesAverageScore(title, Column.USER_SCORE) != null)
                    .limit(5)
                    .map(TitleScore::fromTitle)
                    .collect(Collectors.toList());

            titleScores.add(TitleScore.fromTitle(titleOptional.get()));
            return titleScores;
        }
        return new LinkedList<>();
    }
	
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("games/regions/names")
    public Collection<String> regionListAction() {
        return List.of("europe", "global", "japan", "northAmerica", "other");
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("games/years")//get all years that occur in database
    public List<String> yearsListAction() {
        return repositoryWrapper.getGameDiagramEntryRepository().getYearsList();
    }

}
