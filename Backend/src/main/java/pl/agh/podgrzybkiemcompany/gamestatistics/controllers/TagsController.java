package pl.agh.podgrzybkiemcompany.gamestatistics.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Client;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Tag;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.TagName;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Title;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.projections.TagNameWithOccurrencesCount;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.ClientRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.TagNameRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.TagRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.TitleRepository;

import java.util.Collection;
import java.util.Optional;

@RestController
public class TagsController {

    private final TagRepository tagRepository;
    private final TagNameRepository tagNameRepository;
    private final TitleRepository titleRepository;
    private final ClientRepository clientRepository;

    @Autowired
    public TagsController(TagRepository tagRepository,
                          TagNameRepository tagNameRepository,
                          TitleRepository titleRepository,
                          ClientRepository clientRepository) {
        this.tagRepository = tagRepository;
        this.tagNameRepository = tagNameRepository;
        this.titleRepository = titleRepository;
        this.clientRepository = clientRepository;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/tags/{titleId}")
    public Collection<TagNameWithOccurrencesCount> getTagsOrderedByOccurrencesCount(
            @PathVariable Integer titleId
    )
    {
        return tagRepository.findTagOccurrencesCountForTitle(titleId);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/tags/{titleId}/{username}")
    public Collection<TagNameWithOccurrencesCount> getTagsForClientOrderedByOccurrencesCount(
            @PathVariable Integer titleId,
            @PathVariable String username
    )
    {
        return tagRepository.findTagOccurrencesCountForTitleAndClient(titleId, username);
    }

    @PostMapping("/tags/{titleId}/{username}")
    public void createTag(
            @PathVariable Integer titleId,
            @PathVariable String username,
            @RequestParam(value = "tagName") String tagName
    )
    {
        Optional<Title> title = titleRepository.findById(titleId);
        Optional<Client> client = clientRepository.findByName(username);

        Optional<TagName> name = tagNameRepository.findByName(tagName);

        if (title.isPresent() && client.isPresent()) {
            TagName tagNameToInsert;
            if (name.isEmpty()) {
                tagNameToInsert = new TagName(tagName);
                tagNameRepository.save(tagNameToInsert);
            } else {
                tagNameToInsert = name.get();
            }
            Tag tag = new Tag(tagNameToInsert, title.get(), client.get());
            tagRepository.save(tag);
        }
    }

    @DeleteMapping("/tags/{titleId}/{username}")
    public void deleteTag(
            @PathVariable Integer titleId,
            @PathVariable String username,
            @RequestParam(value = "tagName") String tagName
    )
    {
        tagRepository
                .findTagByTitleUserAndName(titleId, username, tagName)
                .ifPresent(tagRepository::delete);

    }
}
