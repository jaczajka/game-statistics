package pl.agh.podgrzybkiemcompany.gamestatistics.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Game;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.GameRepository;

@Service
public class GameService extends GenericService<Game, Integer> {
}
