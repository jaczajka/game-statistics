package pl.agh.podgrzybkiemcompany.gamestatistics.services;

import org.springframework.stereotype.Service;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Platform;

@Service
public class PlatformService extends GenericService<Platform, Integer> {
}
