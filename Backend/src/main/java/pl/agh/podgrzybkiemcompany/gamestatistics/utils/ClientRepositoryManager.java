package pl.agh.podgrzybkiemcompany.gamestatistics.utils;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Client;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.ClientRepository;

import java.util.Optional;

@Getter
@Service
public class ClientRepositoryManager {

    @Autowired
    private ClientRepository clientRepository;

    public void register(Client client) throws AlreadyRegisteredException {
        final String username = client.getName();
        if (isRegistered(username))
            throw new AlreadyRegisteredException("Client " + username + " already registered");
        clientRepository.save(client);
    }

    public boolean isRegistered(String username) {
        Optional<Client> result = findByUsername(username);
        return result.isPresent();
    }

    public Optional<Client> findByUsername(String username) {
        Iterable<Client> clients = clientRepository.findAll();
        for (Client client : clients) {
            if (hasUsername(client, username))
                return Optional.of(client);
        }
        return Optional.empty();
    }

    private boolean hasUsername(Client client, String pattern) {
        return client.getName() != null && client.getName().equals(pattern);
    }
}
