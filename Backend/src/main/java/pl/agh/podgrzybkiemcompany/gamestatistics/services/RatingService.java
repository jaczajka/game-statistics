package pl.agh.podgrzybkiemcompany.gamestatistics.services;

import org.springframework.stereotype.Service;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Rating;

@Service
public class RatingService extends GenericService<Rating, Integer>{
}
