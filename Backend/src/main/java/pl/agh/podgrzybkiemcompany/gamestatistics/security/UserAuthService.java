package pl.agh.podgrzybkiemcompany.gamestatistics.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Client;
import pl.agh.podgrzybkiemcompany.gamestatistics.utils.ClientRepositoryManager;

import java.util.Optional;

@Service
public class UserAuthService implements UserDetailsService {

    @Autowired
    private ClientRepositoryManager clientRepositoryManager;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        final Optional<Client> client = clientRepositoryManager.findByUsername(userName);
        if (client.isEmpty())
            throw new UsernameNotFoundException("User " + userName + " not registered");
        return new UserData(client.get());
    }
}
