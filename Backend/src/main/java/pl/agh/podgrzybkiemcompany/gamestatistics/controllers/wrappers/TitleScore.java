package pl.agh.podgrzybkiemcompany.gamestatistics.controllers.wrappers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.agh.podgrzybkiemcompany.gamestatistics.importer.Column;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Game;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Title;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class TitleScore {
    private Integer titleId;
    private String titleName;
    private Double criticsScore;
    private Double usersScore;

    public static TitleScore fromTitle(Title title){
        return new TitleScore(
                title.getTitleId(),
                title.getName(),
                TitleScore.getTitlesAverageScore(title, Column.CRITIC_SCORE),
                TitleScore.getTitlesAverageScore(title, Column.USER_SCORE));
    }
    public static Double getTitlesAverageScore(Title title, Column column){
        Stream<Game> gameStream = title.getGames()
                .stream();
        Stream<Double> scoresStream = null;
        if (column.equals(Column.CRITIC_SCORE)){
            scoresStream = gameStream
                    .filter(game -> game.getCriticsScore() != null)
                    .map(Game::getCriticsScore);
        }
        else if (column.equals(Column.USER_SCORE)){
            scoresStream = gameStream
                    .filter(game -> game.getUsersScore() != null)
                    .map(Game::getUsersScore);
        }
        assert scoresStream != null;
        Optional<Double> scoreSum = scoresStream.reduce(Double::sum);
        if (scoreSum.isPresent()){
            return scoreSum.get() / title.getGames().size();
        }
        else{
            return null;
        }
    }
}
