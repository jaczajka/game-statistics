package pl.agh.podgrzybkiemcompany.gamestatistics.dto.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameListRecommendationEntry;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.GameCountQueryBuilder;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.GameRecommendationListQueryBuilder;
import pl.agh.podgrzybkiemcompany.gamestatistics.utils.Page;

import java.util.List;

@Repository
public class GameListRecommendationEntryRepositoryImpl implements GameListRecommendationEntryRepositoryExtension {

    private final GameRecommendationListQueryBuilder listQueryBuilder;
    private final GameCountQueryBuilder countQueryBuilder;

    @Autowired
    public GameListRecommendationEntryRepositoryImpl(GameRecommendationListQueryBuilder queryBuilder, GameCountQueryBuilder countQueryBuilder) {
        this.listQueryBuilder = queryBuilder;
        this.countQueryBuilder = countQueryBuilder;
    }

    @Override
    public Page<GameListRecommendationEntry> getPaginatedList(
            String username,
            String searchPhrase,
            List<String> genres,
            Integer minUsersScore,
            Integer minCriticsScore,
            Integer maxUsersScore,
            Integer maxCriticsScore,
            List<String> developers,
            Integer minScoresCount,
            Integer page,
            Integer perPage,
            List<String> orderBy
    ) {

        List<GameListRecommendationEntry> items = listQueryBuilder
                .setPreferabilitySortCriteria()
                .filterByUsername(username)
                .orderBy(orderBy)
                .setPerPage(perPage)
                .setPage(page)
                .filterByTitlePhrase(searchPhrase)
                .filterByDevelopers(developers)
                .filterByGenres(genres)
                .filterByUsersScore(minUsersScore, maxUsersScore)
                .filterByCriticsScore(minCriticsScore, maxCriticsScore)
                .filterByMinScoresCount(minScoresCount)
                .build()
                .getResultList();
        Long totalCount = countQueryBuilder
                .filterByTitlePhrase(searchPhrase)
                .filterByDevelopers(developers)
                .filterByGenres(genres)
                .filterByUsersScore(minUsersScore, maxUsersScore)
                .filterByCriticsScore(minCriticsScore, maxCriticsScore)
                .filterByMinScoresCount(minScoresCount)
                .build()
                .getSingleResult();
        return new Page<>(items, totalCount, page);
    }
}
