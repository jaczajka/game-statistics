package pl.agh.podgrzybkiemcompany.gamestatistics.dto.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.agh.podgrzybkiemcompany.gamestatistics.controllers.wrappers.SimpleDiagramDataRow;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.GameDiagramQueryBuilder;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.GameYearsListQueryBuilder;

import java.util.List;

@Repository
public class GameDiagramEntryRepositoryImpl implements GameDiagramEntryRepositoryExtension{
    private final GameDiagramQueryBuilder gameDiagramQueryBuilder;
    private final GameYearsListQueryBuilder gameYearsListQueryBuilder;

    @Autowired
    public GameDiagramEntryRepositoryImpl(
            GameDiagramQueryBuilder gameDiagramQueryBuilder,
            GameYearsListQueryBuilder gameYearsListQueryBuilder
    ) {
        this.gameDiagramQueryBuilder = gameDiagramQueryBuilder;
        this.gameYearsListQueryBuilder = gameYearsListQueryBuilder;
    }
    @Override
    public List<SimpleDiagramDataRow> getDiagramData(
            String aggregatedColumn,
            GameDiagramQueryBuilder.AggregateOperation aggregateOperation,
            String platformName,
            String genreName,
            String ratingName,
            List<String> groupings
    ) {
        gameDiagramQueryBuilder.setAggregatedColumn(aggregatedColumn);
        gameDiagramQueryBuilder.setAggregateOperation(aggregateOperation);

        if (platformName != null) {
            gameDiagramQueryBuilder.filterByPlatformName(platformName);
        }
        if (genreName != null) {
            gameDiagramQueryBuilder.filterByGenreName(genreName) ;
        }
        if (ratingName != null) {
            gameDiagramQueryBuilder.filterByRatingName(ratingName);
        }

        gameDiagramQueryBuilder.groupBy(groupings);
        return gameDiagramQueryBuilder.build().getResultList();
    }

    @Override
    public List<String> getYearsList(){
        return gameYearsListQueryBuilder.build().getResultList();
    }

}
