package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString(exclude = {"titles", "preferredBy"})
@Entity
public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer genreId;

    @NotNull
    @Column(unique = true)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "genre")
    private Set<Title> titles = new HashSet<>();


    @JsonIgnore
    @ManyToMany(mappedBy = "preferredGenres")
    private Set<Client> preferredBy = new HashSet<>();
    public void addPreferredByClient(Client client){
        preferredBy.add(client);
    }

    public Genre() {
    }

    public Genre(String name) {
        this.name = name;
    }
}