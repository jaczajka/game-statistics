package pl.agh.podgrzybkiemcompany.gamestatistics.importer;

import org.springframework.stereotype.Component;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.*;
import pl.agh.podgrzybkiemcompany.gamestatistics.services.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

@Component
public class Importer {

    private Map<TableName, GenericService> services;

    private Map<TableName, Class> databaseClasses = Map.of(
            TableName.GENRE, Genre.class,
            TableName.PUBLISHER, Publisher.class,
            TableName.TITLE, Title.class,
            TableName.PLATFORM, Platform.class,
            TableName.RATING, Rating.class,
            TableName.DEVELOPER, Developer.class,
            TableName.GAME, Game.class
    );


    public Importer(
            GenreService genreService,
            PublisherService publisherService,
            TitleService titleService,
            PlatformService platformService,
            RatingService ratingService,
            DeveloperService developerService,
            GameService gameService
    ) {
        services = Map.of(
                TableName.GENRE, genreService,
                TableName.PUBLISHER, publisherService,
                TableName.TITLE, titleService,
                TableName.PLATFORM, platformService,
                TableName.RATING, ratingService,
                TableName.DEVELOPER, developerService,
                TableName.GAME, gameService
        );
    }

    private Map<String, Object> getAlreadyInsertedData(TableName tableName) throws NoSuchMethodException {
        Map<String, Object> alreadyInserted = new HashMap<>();
        final Method getNameMethod = databaseClasses.get(tableName).getMethod("getName");

        services.get(tableName)
                .findAll()
                .forEach(record -> {
                    try {
                        alreadyInserted
                                .put(getNameMethod.invoke(record).toString(),
                                        record);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                });

        return alreadyInserted;
    }

    private List<HashMap> getDataFromCSV() {
        return CSVReader.getData();
    }

    //Used to insert all those objects that have only name in their attributes
    private Object insertToDatabase(String insertedName, TableName tableName) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Object inserted;

        if (!alreadyInsertedMap.get(tableName).containsKey(insertedName)) {
            Constructor constructor = databaseClasses
                    .get(tableName)
                    .getConstructor(new Class[]{String.class});

            inserted = constructor.newInstance(insertedName);
            alreadyInsertedMap.get(tableName).put(insertedName, inserted);
            services.get(tableName).save(inserted);
        } else {
            inserted = alreadyInsertedMap.get(tableName).get(insertedName);
        }
        return inserted;
    }

    private Object insertTitleToDatabase(String insertedName, Map additionalObjects) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Genre genre;
        Publisher publisher;

        genre = (Genre) additionalObjects.get(TableName.GENRE);
        publisher = (Publisher) additionalObjects.get(TableName.PUBLISHER);


        Object inserted;

        if (!alreadyInsertedMap.get(TableName.TITLE).containsKey(insertedName)) {
            Constructor constructor = databaseClasses
                    .get(TableName.TITLE)
                    .getConstructor(new Class[]{});

            inserted = constructor.newInstance();

            Method setGenre = Title.class.getMethod("setGenre", Genre.class);
            setGenre.invoke(inserted, genre);

            Method setPublisher = Title.class.getMethod("setPublisher", Publisher.class);
            setPublisher.invoke(inserted, publisher);

            Method setName = Title.class.getMethod("setName", String.class);
            setName.invoke(inserted, insertedName);

            alreadyInsertedMap.get(TableName.TITLE).put(insertedName, inserted);
            services.get(TableName.TITLE).save(inserted);
        } else {
            inserted = alreadyInsertedMap.get(TableName.TITLE).get(insertedName);
        }
        return inserted;
    }

    private Set<Developer> insertDevelopersToDatabase(ArrayList<String> developerNames) {
        Developer developer;
        Map<String, Object> alreadyInsertedDevelopers = alreadyInsertedMap.get(TableName.DEVELOPER);

        if (developerNames.size() == 0) {
            String developerName = "Not known";
            if (!alreadyInsertedDevelopers.containsKey(developerName)) {
                developer = new Developer("Not known");

                alreadyInsertedDevelopers.put(developerName, developer);
                services.get(TableName.DEVELOPER).save(developer);
            } else {
                developer = (Developer) alreadyInsertedDevelopers.get(developerName);
            }
            return Set.of(developer);
        } else {
            Set<Developer> insertedDevelopers = new HashSet<>();

            for (String developerName : developerNames) {
                String developerNameTrimmed = developerName.replaceAll("^\\s+", "");
                developerNameTrimmed = developerNameTrimmed.replaceAll("\\s+$", "");
                developerNameTrimmed = developerNameTrimmed.replaceAll("\"+$", "");

                if (developerNameTrimmed.equals("")) {
                    developerNameTrimmed = "Not Known";
                }
                if (!alreadyInsertedDevelopers.containsKey(developerNameTrimmed)) {
                    developer = new Developer(developerNameTrimmed);

                    alreadyInsertedDevelopers.put(developerNameTrimmed, developer);
                    services.get(TableName.DEVELOPER).save(developer);
                } else {
                    developer = (Developer) alreadyInsertedDevelopers.get(developerNameTrimmed);
                }
                insertedDevelopers.add(developer);
            }

            return insertedDevelopers;
        }
    }

    private void setGameProperties(Game game, HashMap<String, Object> row) {
        String yearOfRelease = (row.get("Year_of_Release") != null ? row.get("Year_of_Release").toString() : null);
        Double NA_Sales = (row.get("NA_Sales") != null ? Double.parseDouble(row.get("NA_Sales").toString()) : Double.NaN);
        Double EU_Sales = (row.get("EU_Sales") != null ? Double.parseDouble(row.get("EU_Sales").toString()) : Double.NaN);
        Double JP_Sales = (row.get("JP_Sales") != null ? Double.parseDouble(row.get("JP_Sales").toString()) : Double.NaN);
        Double otherSales = (row.get("Other_Sales") != null ? Double.parseDouble(row.get("Other_Sales").toString()) : Double.NaN);
        Double globalSales = (row.get("Global_Sales") != null ? Double.parseDouble(row.get("Global_Sales").toString()) : Double.NaN);

        Double criticScore;
        Integer criticCount;
        Double userScore;
        Integer userCount;
        try {
            criticScore = (row.get("Critic_Score") != null && !row.get("Critic_Score").toString().isEmpty() ?
                    Double.parseDouble(row.get("Critic_Score").toString()) : null);
        } catch (NumberFormatException e) {
            criticScore = null;
        }
        try {
            criticCount = (row.get("Critic_Count") != null && !row.get("Critic_Score").toString().isEmpty() ?
                    Integer.parseInt(row.get("Critic_Count").toString()) : null);
        } catch (NumberFormatException e) {
            criticCount = null;
        }
        try {
            userScore = (row.get("User_Score") != null && !row.get("User_Score").toString().isEmpty() ?
                    Double.parseDouble(row.get("User_Score").toString()) : null);
        } catch (NumberFormatException e) {
            userScore = null;
        }
        try {
            userCount = (row.get("User_Count") != null && !row.get("User_Count").toString().isEmpty() ?
                    Integer.parseInt(row.get("User_Count").toString()) : null);
        } catch (NumberFormatException e) {
            userCount = null;
        }

        game.setYearOfRelease(yearOfRelease);

        game.setNorthAmericaSales(NA_Sales);
        game.setEuropeSales(EU_Sales);
        game.setJapanSales(JP_Sales);
        game.setOtherSales(otherSales);
        game.setGlobalSales(globalSales);

        game.setCriticsScore(criticScore);
        game.setCriticsCount(criticCount);
        game.setUsersScore(userScore);
        game.setUsersCount(userCount);

    }

    private Map<TableName, Map<String, Object>> alreadyInsertedMap =
            new HashMap<>();

    public void importData() throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {

        Arrays.stream(TableName.values())
                .filter(tableName -> tableName != TableName.GAME)
                .forEach(tableName -> {
                    try {
                        alreadyInsertedMap.put(tableName, getAlreadyInsertedData(tableName));
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                });

        List<HashMap> data = getDataFromCSV();

        Set<Game> alreadyInsertedGames = new HashSet<>();
        services.get(TableName.GAME).findAll().forEach(e -> alreadyInsertedGames.add((Game) e));

        List<Game> gamesToInsert = new LinkedList<>();

        System.out.println("Insertion starts...");
        for (HashMap row : data) {
            Map<TableName, Object> titleAttributes = new HashMap<>();


            String genreName = row.get("Genre") != null ? row.get("Genre").toString() : "Not Known";
            Genre genre = (Genre) insertToDatabase(genreName, TableName.GENRE);


            String publisherName = row.get("Publisher") != null ? row.get("Publisher").toString() : "Not Known";
            Publisher publisher = (Publisher) insertToDatabase(publisherName, TableName.PUBLISHER);


            titleAttributes.put(TableName.GENRE, genre);
            titleAttributes.put(TableName.PUBLISHER, publisher);

            String titleName = row.get("Name") != null ? row.get("Name").toString() : "Not Known";
            Title title = (Title) insertTitleToDatabase(titleName, titleAttributes);


            String platformName = row.get("Platform") != null ? row.get("Platform").toString() : "Not Known";
            Platform platform = (Platform) insertToDatabase(platformName, TableName.PLATFORM);


            String ratingName = row.get("Rating") != null ? row.get("Rating").toString() : "Not Known";
            String ratingNameRedone = ratingName.replace("\"", "");//case when rating contains " signs
            Rating rating = (Rating) insertToDatabase(ratingNameRedone, TableName.RATING);

            ArrayList<String> developerNames = (ArrayList<String>) row.get("Developer");
            Set<Developer> developers = insertDevelopersToDatabase(developerNames);

            Game game = new Game();
            game.setTitle(title);
            game.setPlatform(platform);
            game.setRating(rating);
            game.setDevelopers(developers);
            setGameProperties(game, row);

            gamesToInsert.add(game);
        }

        System.out.println("Insertion of games starts...");

        Set<List<String>> alreadyInsertedPlatformsTitles = new HashSet<>();
        alreadyInsertedGames.forEach(game -> alreadyInsertedPlatformsTitles
                .add(List
                        .of(game.getTitle().getName()
                                , game.getPlatform().getName()
                                , game.getRating().getName())));


        for (Game game : gamesToInsert) {
            if (!alreadyInsertedPlatformsTitles.contains(List.of(game.getTitle().getName()
                    , game.getPlatform().getName()
                    , game.getRating().getName()))) {

                alreadyInsertedPlatformsTitles.add(List.of(game.getTitle().getName()
                        , game.getPlatform().getName()
                        , game.getRating().getName()));
                services.get(TableName.GAME).save(game);
            }
        }

        System.out.println("Done!");
    }
}