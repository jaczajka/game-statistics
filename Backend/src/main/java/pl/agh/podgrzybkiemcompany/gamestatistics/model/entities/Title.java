package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString(exclude = {"games", "favouriteOf", "tags"})
@Entity
public class Title {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer titleId;

    @NotNull
    private String name;

    @ManyToOne
    @NotNull
    private Genre genre;

    @ManyToOne
    @NotNull
    private Publisher publisher;

    @OneToMany(mappedBy = "title")
    private Set<Game> games = new HashSet<>();

    @JsonIgnore
    @ManyToMany(mappedBy = "favouriteTitles")
    private Set<Client> favouriteOf = new HashSet<>();
    public void addFavouriteOfClient(Client client){
        favouriteOf.add(client);
    }

    @JsonIgnore
    @OneToMany(mappedBy = "relatedTitle")
    private Set<Tag> tags = new HashSet<>();
    public void addTag(Tag tag){
        this.tags.add(tag);
    }
}
