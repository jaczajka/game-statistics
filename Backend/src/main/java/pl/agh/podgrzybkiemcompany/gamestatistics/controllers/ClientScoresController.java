package pl.agh.podgrzybkiemcompany.gamestatistics.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.RepositoryWrapper;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.*;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.projections.MinMaxScoreForClient;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.ClientScoreRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories.GameRepository;
import pl.agh.podgrzybkiemcompany.gamestatistics.utils.ClientRepositoryManager;
import pl.agh.podgrzybkiemcompany.gamestatistics.utils.Page;

import java.util.Optional;
import java.util.Set;

@RestController
public class ClientScoresController {
    private final ClientScoreRepository clientScoreRepository;
    private final ClientRepositoryManager clientRepository;
    private final GameRepository gameRepository;

    private final RepositoryWrapper repositoryWrapper;

    @Autowired
    public ClientScoresController(ClientScoreRepository clientScoreRepository,
                                  ClientRepositoryManager clientRepository,
                                  GameRepository gameRepository,
                                  RepositoryWrapper repositoryWrapper) {
        this.clientScoreRepository = clientScoreRepository;
        this.clientRepository = clientRepository;
        this.gameRepository = gameRepository;
        this.repositoryWrapper = repositoryWrapper;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("games/client-scores")
    public Double getScore(
            @RequestParam(value = "gameId") Integer gameId,
            @RequestParam(value = "username") String username
    ) {
        System.out.println(gameId + " "+ username);
        Game game = gameRepository.findById(gameId).get();
        if (clientRepository.findByUsername(username).isEmpty())
            return 0.0;
        Client client = clientRepository.findByUsername(username).get();

        Optional<ClientScore> currentScore = getCurrentScore(game, client);
        if (currentScore.isPresent()) {
            return currentScore.get().getValue();
        }
        return 0.0;
    }


    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("games/client-scores")
    public void submitScore(
            @RequestParam(value = "value") Double scoreValue,
            @RequestParam(value = "gameId") Integer gameId,
            @RequestParam(value = "username") String username
    ) {
        Game game = gameRepository.findById(gameId).get();
        Client client = clientRepository.findByUsername(username).get();

        Optional<ClientScore> currentScore = getCurrentScore(game, client);
        if (currentScore.isPresent()) {
            currentScore.get().setValue(scoreValue);
            clientScoreRepository.save(currentScore.get());
        } else {
            ClientScore score = new ClientScore(scoreValue);
            score.setScoredGame(game);
            score.setScoreIssuer(client);
            clientScoreRepository.save(score);
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/games/client-scores/{username}")
    public Page<GradedGameListEntry> gradedGameListAction(
            @PathVariable String username,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "perPage", defaultValue = "10") Integer perPage
    ) {
        return repositoryWrapper.getGradedGameListEntryRepository().getPaginatedList(
                username,
                page,
                perPage
        );
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/games/min-max-scores")
    public MinMaxScoreForClient getMinMaxScoresForClient(
            @RequestParam(value = "username") String username
    )
    {
        ClientScore minScore
            = clientScoreRepository.findMinScoreForClient(username);
        ClientScore maxScore
            = clientScoreRepository.findMaxScoreForClient(username);
        return new MinMaxScoreForClient(minScore.getValue(), maxScore.getValue());
    }

    private Optional<ClientScore> getCurrentScore(Game game, Client client) {
        Set<ClientScore> gamesScores = game.getRegisteredClientsScores();
        Set<ClientScore> clientsScores = client.getClientScores();

        return gamesScores.stream().filter(clientsScores::contains).findFirst();
    }
}
