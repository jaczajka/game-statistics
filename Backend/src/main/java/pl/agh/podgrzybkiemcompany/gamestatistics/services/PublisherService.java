package pl.agh.podgrzybkiemcompany.gamestatistics.services;

import org.springframework.stereotype.Service;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Publisher;

@Service
public class PublisherService extends GenericService<Publisher, Integer>{
}
