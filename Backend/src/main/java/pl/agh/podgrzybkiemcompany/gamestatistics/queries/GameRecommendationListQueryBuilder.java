package pl.agh.podgrzybkiemcompany.gamestatistics.queries;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameListRecommendationEntry;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GameRecommendationListQueryBuilder extends UserSpecificListQueryBuilder<GameListRecommendationEntry> {

    private static final String DESCENDING_SORT_ORDER = "desc";
    private static final String PREFERABILITY_COLUMN_NAME = "preferability";

    @Autowired
    public GameRecommendationListQueryBuilder(EntityManager entityManager, SortingOrderAdapter sortingAdapter) {
        super(entityManager, sortingAdapter, GameListRecommendationEntry.class, GameListRecommendationEntry.class);
    }

    public GameRecommendationListQueryBuilder setPreferabilitySortCriteria() {
        List<String> preferabilitySort = Arrays.asList(PREFERABILITY_COLUMN_NAME, DESCENDING_SORT_ORDER);
        orderBy(preferabilitySort);
        return this;
    }

}