package pl.agh.podgrzybkiemcompany.gamestatistics.importer;

public enum TableName {
    GENRE,
    PUBLISHER,
    TITLE,
    PLATFORM,
    RATING,
    DEVELOPER,
    GAME
}
