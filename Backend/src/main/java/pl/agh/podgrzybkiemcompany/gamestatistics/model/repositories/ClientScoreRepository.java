package pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.ClientScore;

public interface ClientScoreRepository extends CrudRepository<ClientScore, Integer> {
    @Query(value = "SELECT * "                                                  +
                   "FROM client_score "                                         +
                   "WHERE score_issuer_client_id IN (SELECT client_id "         +
                                                    "FROM client "              +
                                                    "WHERE name = :username) "  +
                   "ORDER BY value "                                            +
                   "LIMIT 1",
           nativeQuery = true)
    ClientScore findMinScoreForClient(
            @Param("username") String username
    );

    @Query(value = "SELECT * "                                                 +
                   "FROM client_score "                                        +
                   "WHERE score_issuer_client_id IN (SELECT client_id "        +
                                                    "FROM client "             +
                                                    "WHERE name = :username) " +
                   "ORDER BY value DESC "                                      +
                   "LIMIT 1",
           nativeQuery = true)
    ClientScore findMaxScoreForClient(
            @Param("username") String username
    );
}
