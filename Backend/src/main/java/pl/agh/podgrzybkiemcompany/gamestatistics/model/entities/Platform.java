package pl.agh.podgrzybkiemcompany.gamestatistics.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString(exclude = {"games"})
@Entity
public class Platform {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer platformId;

    @NotNull
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "platform")
    private Set<Game> games = new HashSet<>();

    public Platform() {
    }

    public Platform(String name) {
        this.name = name;
    }
}
