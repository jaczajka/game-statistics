package pl.agh.podgrzybkiemcompany.gamestatistics.model.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Title;

import java.util.Collection;

@Repository
public interface TitleRepository extends CrudRepository<Title, Integer> {
    @Query(value = "SELECT t.title_id, "                                        +
                          "t.name, "                                            +
                          "t.genre_genre_id, "                                  +
                          "t.publisher_publisher_id "                           +
                   "FROM title AS t "                                           +
                     "JOIN client_favourite_titles AS cft "                     +
                       "ON title_id = favourite_titles_title_id "               +
                   "WHERE favourite_of_client_id IN (SELECT client_id "         +
                                                    "FROM client AS c "         +
                                                    "WHERE c.name = :username)",
            nativeQuery = true)
    Collection<Title> findByFavouritesOfClient(
            @Param("username") String username
    );
}
