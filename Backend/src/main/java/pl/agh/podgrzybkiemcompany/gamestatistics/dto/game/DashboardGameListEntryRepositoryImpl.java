package pl.agh.podgrzybkiemcompany.gamestatistics.dto.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.GameCountQueryBuilder;
import pl.agh.podgrzybkiemcompany.gamestatistics.queries.UserSpecificListQueryBuilder;
import pl.agh.podgrzybkiemcompany.gamestatistics.utils.Page;

import java.util.List;

@Repository
public abstract class DashboardGameListEntryRepositoryImpl<T>
        implements DashboardGameListEntryRepositoryExtension<T> {

    private final UserSpecificListQueryBuilder<T> listQueryBuilder;
    private final GameCountQueryBuilder countQueryBuilder;

    @Autowired
    public DashboardGameListEntryRepositoryImpl(UserSpecificListQueryBuilder<T> queryBuilder, GameCountQueryBuilder countQueryBuilder) {
        this.listQueryBuilder = queryBuilder;
        this.countQueryBuilder = countQueryBuilder;
    }

    @Override
    public Page<T> getPaginatedList(
            String username,
            Integer page,
            Integer perPage
    )
    {
        List<T> items = listQueryBuilder
                .filterByUsername(username)
                .setPerPage(perPage)
                .setPage(page)
                .build()
                .getResultList();
        Long totalCount = countQueryBuilder
                .build()
                .getSingleResult();
        return new Page<>(items, totalCount, page);
    }
}
