package pl.agh.podgrzybkiemcompany.gamestatistics.queries;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.GameListEntry;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GameCountQueryBuilder extends AbstractGameQueryBuilder<Long, GameListEntry> {

    private final EntityManager entityManager;

    @Autowired
    public GameCountQueryBuilder(EntityManager entityManager) {
        super(entityManager, Long.class, GameListEntry.class);
        this.entityManager = entityManager;
    }

    public TypedQuery<Long> build() {
        CriteriaQuery<Long> dbQuery = query
                .select(queryBuilder.countDistinct(from))
                .where(queryBuilder.and(wheres.toArray(new Predicate[]{})));
        TypedQuery<Long> typedQuery = entityManager.createQuery(dbQuery);
        parameters.forEach(typedQuery::setParameter);
        reset();
        return typedQuery;
    }
}
