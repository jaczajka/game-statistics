package pl.agh.podgrzybkiemcompany.gamestatistics.dto.game;

import pl.agh.podgrzybkiemcompany.gamestatistics.utils.Page;

public interface DashboardGameListEntryRepositoryExtension<T> {
    Page<T> getPaginatedList(
            String username,
            Integer page,
            Integer perPage
    );
}
