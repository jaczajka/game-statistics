package pl.agh.podgrzybkiemcompany.gamestatistics;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import pl.agh.podgrzybkiemcompany.gamestatistics.model.entities.Platform;
import pl.agh.podgrzybkiemcompany.gamestatistics.services.PlatformService;


import java.util.Optional;

@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
public class PlatformServiceTests {

    @Autowired
    private PlatformService platformService;

    @Test
    public void shouldReturnGivenNameTest(){

    }

}
