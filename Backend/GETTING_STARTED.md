# Getting Started

### Baza danych

* Zainstaluj postgresql co najmniej wersję 10
* Powinien już istnieć user postgres z hasłem postgres
* Wykonaj w cmd komendę psql -U postgres i w konsoli bazy stwórz bazę danych:
```sql
CREATE DATABASE game_statistics;
``` 
* Jeśli pojawią się problemy z połączeniem to prawdopodobnie [to badziewie](https://stackoverflow.com/questions/18664074/getting-error-peer-authentication-failed-for-user-postgres-when-trying-to-ge):

### Gradle
* Zainstaluj co najmniej wersję 4.10 (a najlepiej w ogóle 6+)
* Jeśli zaimportowałeś jako projekt Gradle, to wszystko masz, jeśli nie, dodaj konfigurację do gradle'a
* Jeśli intellij z niezrozumiałych powodów płacze za Android SDK, wejdź w Plugins w zakładkę installed i kliknij w disable koło supportu do Androida

### Serwer
Prawie nic nie musisz robić - zbuduj projekt i kliknij zieloną strzałkę koło maina albo taska bootRun w gradle.build, a jeśli wolisz oldschoolowo, to napisz gradle bootRun w konsoli

Po wejściu w przeglądarce na localhost:8080 powinieneś zobaczyć naszą wspaniałą aplikację

### Import danych
Odpalić serwer restowy (podpunkt "Serwer"), wpisać w konsoli **import_data** i cierpliwie czekać...

### Pisanie testów
Nad każdą klasą z testami trzeba zawrzeć poniżną adnotację:
```java
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
``` 
