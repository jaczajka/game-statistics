# Getting Started

## Gitlab-runner
Gitlab-runner umożliwia lokalne uruchamianie pipeline-ów CI/CD. Można go
używać jako obraz docker-owy:
1. Zainstaluj i uruchom [docker-a](https://docs.docker.com/install/).
2. Wykonaj:
```
docker run -d --name gitlab-runner --restart always -v PATH1:PATH2 -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
```
gdzie:
* `PATH1` określa ścieżkę do projektu game-statistics.
* `PATH2` określa ścieżkę, pod którą obraz docker-owy widzieć będzie `PATH1`.

Przykładowo:
```
docker run -d --name gitlab-runner --restart always -v ~/Projects/game-statistics:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
```
Komenda ta:
* Pobierze obraz `gitlab/gitlab-runner:latest` (jeżeli jeszcze go nie ma).
* Zamontuje podane ścieżki.
* Uruchomi instancję `gitlab-runner`.
3.  Wejdź do katalogu projektu na działającej instancji:
```
docker exec -it /gitlab-runner /bin/bash
cd PATH2
```
4. Uruchom pipeline (przykładowo stage build):
```
gitlab-runner exec shell build
```
Uwagi:
* Zamiast `shell` możemy użyć innych wykonawców (nie testowałem tego):
```
gitlab-runner exec --help
```
* Zgodnie z instrukcją, gitlab-runner trzeba zarejestrować:
```
gitlab-runner register
```
Potrzebne dane (coordinator URL i token) można wziąć z
```
Settings > CI/CD > Runners > Set up a specific Runner manually
```
Zadziałało mi jednak bez rejestrowania, więc nie wiem na ile jest to potrzebne.