const fs = require('fs');

const envPath = '.env.' + process.env.MODE;
const config = fs.readFileSync(envPath, 'utf8');
console.log(config);
fs.writeFileSync('./src/environment.ts', `export const CONFIG = ${config};`);
console.log('Environment file created');
