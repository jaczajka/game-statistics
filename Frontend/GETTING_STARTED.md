# Getting Started

### NPM

* Zainstaluj npm wersję 12+
* Jeśli masz inną wersję i chcesz je sprawnie zmieniać, użyj:
```
nvm install 12.0
nvm use 12.0
``` 
* Zainstaluj zależności wykonując:
```
npm install
```

### Startowanie projektu
* Aby wystartować projekt użyj komendy
```
npm run watch
```
* Jeśli chcesz wyczyścić projekt użyj
```
npm run clean
```
Powinna być już dostępna strona główna (z prawie niczym :P), linki i możliwość wejścia na kilka podstron:
* game-list
* game-details/:id
* login-page

