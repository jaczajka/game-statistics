import {ClientScoresEntry} from "./client-scores-entry";

export class ClientScoresPage {
  items: Array<ClientScoresEntry>;
  totalCount: number;
  page: number;
}
