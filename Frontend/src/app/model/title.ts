import {Publisher} from "./publisher";
import {Genre} from "./genre";
import {Game} from "./game";

export class Title {
  titleId: number;
  name: string;
  genre: Genre;
  publisher: Publisher;
  games: Array<Game>
}
