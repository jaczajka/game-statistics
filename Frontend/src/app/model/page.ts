import {GameListEntry} from "./game-list-entry";

export class Page {
  items: Array<GameListEntry>;
  totalCount: number;
  page: number;
}
