import {Platform} from "./platform";
import {Developer} from "./developer";
import {Rating} from "./rating";

export class Game {
  gameId: number;
  yearOfRelease: number;
  northAmericaSales: number;
  europeSales: number;
  japanSales: number;
  otherSales: number;
  globalSales: number;
  criticsScore: number;
  criticsCount: number;
  usersScore: number;
  usersCount: number;
  platform: Platform;
  developers: Array<Developer>;
  rating: Rating;
}
