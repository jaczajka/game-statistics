export class ClientScoresEntry {
  titleId: number;
  title: string;
  genreId: number;
  genre: string;
  givenScore: string;
  platform: string;
  developers: string;
}
