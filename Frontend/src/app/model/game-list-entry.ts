export class GameListEntry {
  titleId: number;
  title: string;
  genreId: number;
  genre: string;
  developers: string;
  averageUsersCount: number;
  averageUsersScore: number;
  averageCriticsCount: number;
  averageCriticsScore: number;
}
