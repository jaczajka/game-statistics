import { Component } from '@angular/core';
import {AuthService} from "../service/auth.service";
import {Router} from '@angular/router';

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']

})
export class LoginPageComponent {
  title = 'Login';
  errCommunicate: string;

  constructor(private authService: AuthService, private router: Router){
  }

  login(){

    let username = (<HTMLInputElement>document.getElementById("username")).value;
    let password = (<HTMLInputElement>document.getElementById("password")).value;

    if (username === '') {
      this.errCommunicate = "Podaj nazwę użytkownika";
      return;
    }
    else if (password === '') {
      this.errCommunicate = "Podaj hasło";
      return;
    }

    this.authService.login(username, password).subscribe(data => {
        this.errCommunicate = "";

        this.authService.isLogged = true;
        this.authService.username = username;
        this.authService.password = password;
        this.authService.jwt = data.jwt;

        this.authService.saveStateToCookies();

        this.router.navigateByUrl('/game-list');
      },
      err => {
        this.errCommunicate = "Błędny login lub hasło"
      }
    );
  }



}
