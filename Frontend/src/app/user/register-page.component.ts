import { Component } from '@angular/core';
import {AuthService} from "../service/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent {
  title = 'Register';
  errCommunicate: string;

  constructor(private authService: AuthService, private router: Router){
  }

  register() {

    let username = (<HTMLInputElement>document.getElementById("username")).value;
    let password = (<HTMLInputElement>document.getElementById("password")).value;
    let passwordConfirm = (<HTMLInputElement>document.getElementById("password-confirm")).value;

    if (username === '') {
      this.errCommunicate = "Podaj nazwę użytkownika";
      return;
    }
    else if (password === '') {
      this.errCommunicate = "Podaj hasło";
      return;
    } else if (password !== passwordConfirm) {
      this.errCommunicate = "Podane hasła są różne";
      return;
    }

    this.authService.register(username, password).subscribe(data => {
        this.errCommunicate = "";

        this.router.navigateByUrl('/login');
      },
      err => {
        this.errCommunicate = "Podana nazwa jest już zajęta"
      }
    );
  }
}
