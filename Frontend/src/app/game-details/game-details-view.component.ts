import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogClose, MatDialogConfig} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {GameService} from "../service/game.service";
import {AuthService} from "../service/auth.service";
import {Title} from "../model/title";
import {Developer} from "../model/developer";
import {TagsDialogComponent, DialogData} from "./tags_view_dialog/tags-dialog.component"
import {FormBuilder} from "@angular/forms";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TagService} from "../service/tag.service";
@Component({
  selector: 'game-details-view',
  templateUrl: './game-details-view.component.html',
  styleUrls: ['./game-details-view.component.scss']
})

export class GameDetailsViewComponent {
  titlesSimilarityColumns: string[] = ["genre", "publisher"]
  titleId: number;
  title: Title;
  gameScores = new Map<number, number>();
  isFavourite: boolean;
  favouritesRatio: number;
  loggedInfo: string;
  orderedPlatforms : string[] = ["PC","PS3","X360","PS2","PS4","XOne","Wii","NES","GB","DS","SNES","GBA","3DS","N64","PS","XB","2600","PSP","WiiU","GC","GEN","DC","PSV","SAT","SCD","WS","NG","TG16","3DO","GG","PCFX"]
  visible : boolean[] = [false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false];

  constructor(private route: ActivatedRoute, private client: HttpClient,
              private gameService: GameService, public authService: AuthService,
              public dialog: MatDialog, private tagService: TagService) {
    this.titleId = this.route.snapshot.params.id;
  }

  currentlyViewedTitleAndUsername : DialogData;
  openDialog(): void{
    if (! this.authService.isLogged){
      return;
    }
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    dialogConfig.height ='70%';
    dialogConfig.data = {
      username: this.authService.username,
      jwt: this.authService.jwt,
      titleId: this.titleId
    }

    const dialogRef = this.dialog.open(TagsDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.displayTags();
      this.currentlyViewedTitleAndUsername = result;
  })}

  ngOnInit() {
    this.loadGameData();
    this.displayTags();

    if(!this.authService.isLogged) {
      this.loggedInfo = "Opcja dla zalogowanych";
    }
    else {
      this.loggedInfo = "";
    }
  }


  loadGameData() {
    return this.gameService.getOne(this.titleId).subscribe((data: Title) => {
      this.title = data;
      this.loadUserScore()
      this.loadUserFavourite()
      this.loadFavouritesRatio()
    })
  };

  addRating(value: number, gameId: number){
    this.gameScores.set(gameId, value)
    this.gameService.postUserScore(value, this.authService.username, gameId, this.authService.jwt).subscribe(data => {});
  }

  onToggle(platform: string) {
    let index = this.orderedPlatforms.findIndex(name => name === platform);
    if (index > -1) {
      this.visible[index] = !this.visible[index];
    }
  }

  loadUserScore(){
    for (let game of this.title.games) {
      this.gameService.getUserScore(this.authService.username, game.gameId).subscribe((data:number) => {
        this.gameScores.set(game.gameId, data)
      })
    }
  }

  loadUserFavourite(){
    return this.gameService.getUserFavourite(this.authService.username, this.titleId).subscribe((data: boolean) => {
      this.isFavourite = data;
    })
  }

  loadFavouritesRatio(){
    return this.gameService.getFavouritesRatio(this.titleId).subscribe((data: number) => {
      this.favouritesRatio = +data.toPrecision(2);
    })
  }

  addToFavourites(){
    if(this.authService.isLogged) {
      this.isFavourite = !this.isFavourite;
      this.gameService.postUserFavourite(this.authService.username, this.titleId, this.authService.jwt).subscribe(data => {
      });
    }
  }

  gameForPlatform(platform: string) {
    return this.title.games.find(game => game.platform.name === platform);
  }

  hasAnyPlatform() {
    return this.title.games.find(game => this.orderedPlatforms.includes(game.platform.name));  }

  displayScore = (score: number) => score ? score + '/10' : 'nieznana';
  displayDevelopers = (developers: Array<Developer>) => developers.map(d => d.name).join(", ");



  tagsLoaded = false;
  mostPopularTags = "";
  displayTags(){
    this.tagService.getTags(this.titleId).subscribe(data => {
      let tags = new Array();

      data.forEach(function(entry: any){
        tags.push(entry.tagName);
      });

      if (tags.length > 4){
        tags = tags.slice(0, 4);
      }

      this.mostPopularTags = tags.join(", ");
      this.tagsLoaded = true;
    });






  }
}
