import {Component, Input} from "@angular/core";
import {GameService} from "../../service/game.service";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
  selector: 'similar-titles-diagram',
  templateUrl: './similar-titles-diagram.component.html',
  styleUrls: ['./similar-titles-diagram.component.scss'],
})

export class SimilarTitlesDiagramComponent {
  @Input() similarityColumn: string;
  @Input() titleId: number;

  isLoaded = false;
  resultsFound = false;

  titleIds: any = [];
  titleList: any = [];
  titleNames: any = [];
  userScores: any = [];
  criticScores: any = [];
  numberLabels: any = [];

  public translateName() {
    if (this.similarityColumn === "genre")
      return "tej samej kategorii";

    return "tego samego wydawcy"
  }

  private barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    spanGaps: true,
    scales: {
      xAxes: [ {
        scaleLabel: {
          display: true,
          labelString: 'Gra'
        },
      } ],
      yAxes: [{
        id: 'critics',
        type: 'linear',
        position: 'left',
        scaleLabel: {
          display: true,
          labelString: 'Ocena'
        },
        ticks: {
          beginAtZero: true,
          max: 10,
          stepSize: 2,
        }
      }, {
        display: false,
        id: 'users',
        type: 'linear',
        position: 'right',
        ticks: {
          beginAtZero: true,
          max: 10,
        }
      }]
    }
  };

  barChartLabels: any[];
  private barChartType: string = 'bar';
  private barChartLegend: boolean = true;

  barChartData: any[] = [
    { data: [], backgroundColor: "#077F07", label: 'Ocena krytyków'},
    { data: [], hoverColor: "#077F07", backgroundColor: "#FFAB0F", label: 'Ocena użytkowników'}
  ];

  // events
  private chartClicked(e: any): void {
    console.log(e);
  }

  private chartHovered(e: any): void {
    console.log(e);
  }

  private getNewLabelsList(array: any, chosenValue: String) : Array<string> {
    let newLabelsList = new Array();

    //insert all values except chosen one
    array.forEach(function(entry: any){
      if (entry != chosenValue){
        newLabelsList.push(entry);
      }
    });

    newLabelsList.sort();
    newLabelsList.unshift(chosenValue);

    return newLabelsList;
  }


  constructor(private gameService: GameService, private route: ActivatedRoute, private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  loadDiagram() {
    this.gameService.getSimilarityDiagram(this.similarityColumn, this.titleId).subscribe( (data: {}) => {
      this.titleList = data;
      let titleNames = new Array();
      let userData = new Array();
      let criticData = new Array();
      let titleIds = new Array();
      let numberLabels = new Array();

      var i = 1;

      this.titleList.forEach(function(entry: any){

        if(entry.usersScore !== null && entry.criticsScore !== null) {
          titleNames.push(entry.titleName);
          userData.push(entry.usersScore);
          criticData.push(entry.criticsScore);
          titleIds.push(entry.titleId);
          numberLabels.push(i)
          i++;
        }
      });

      this.titleIds = titleIds;
      this.titleNames = titleNames;
      this.userScores = userData.map(x => x.toFixed(2));
      this.criticScores = criticData.map(x => x / 10).map(x => x.toFixed(2));
      this.numberLabels = numberLabels;

      this.barChartLabels = Array.from(this.titleNames);
      this.barChartData[0] = { data: this.userScores, hoverBackgroundColor:"#FFC52F", backgroundColor: "rgb(255,171,15,0.9)",
        hoverBorderColor:"#FFAB0F", borderColor: "#FFAB0F", label: 'Ocena użytkowników', yAxisID: 'users'};
      this.barChartData[1] = { data: this.criticScores, hoverBackgroundColor:"#4DB243", backgroundColor: "rgb(7,127,7,0.9)",
        hoverBorderColor:"#077F07", borderColor: "#077F07", label: 'Ocena krytyków', yAxisID: 'critics'};
      this.isLoaded = true;

      if (this.userScores.length > 0 || this.criticScores.length > 0) {
        this.resultsFound = true;
      }
    })
  }

  navigateToGame(i: number){
    var gameId = this.titleIds[i];
    this.router.navigateByUrl('/game-details/'+gameId)
  }

  ngOnInit() {
    this.loadDiagram();
  }
}
