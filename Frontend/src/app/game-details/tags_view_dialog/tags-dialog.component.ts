import {Component, Inject} from "@angular/core";
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {TagService} from '../../service/tag.service'
import {Page} from "../../model/page";
import {Tag} from "../../model/tag";


export interface DialogData {
  username: string;
  titleId: number;
  jwt: string;
}

@Component({
  selector: 'tags-dialog',
  templateUrl: './tags-dialog.component.html',
  styleUrls: ['./tags-dialog.component.scss']
})
export class TagsDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<TagsDialogComponent>,
    private tagService: TagService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
  }

  ngOnInit() {
    this.loadTags();
    this.loadMyTags();
  }

  tagsLoaded = false;
  tags: any[] = [];

  myTagsLoaded = false;
  myTags: any[] = [];

  getTags() {
    return this.tags;
  }

  getMyTags() {
    return this.myTags;
  }

  isInMyTags(tag: string) {
    return this.myTags.indexOf(tag) > -1;
  }

  inputTagName: string;
  insertedAlreadyExistingTag = false;
  insertedEmptyTag = false;

  addInputTag() {
    this.addTag(this.inputTagName);
  }

  addTag(tag: string) {
    //clear error labels:
    this.insertedEmptyTag = false;
    this.insertedAlreadyExistingTag = false;

    if (!this.isInMyTags(tag)) {
      if (tag !== "" && tag != undefined) {
        this.myTags.push(tag);

        this.tagService.postUsersTag(this.data.titleId, this.data.username, tag, this.data.jwt).subscribe(data => {
          this.loadTags();
          this.loadMyTags();
        });
      } else {
        this.insertedEmptyTag = true;
      }
    } else {
      this.insertedAlreadyExistingTag = true;
    }
  }

  delTag(tag: string) {
    const index = this.myTags.indexOf(tag);
    if (index > -1) {
      this.myTags.splice(index, 1);

      this.tagService.deleteUsersTag(this.data.titleId, this.data.username, tag, this.data.jwt).subscribe(data => {
        this.loadTags();
        this.loadMyTags();
      });
    }
  }

  delInputTag() {
    this.delTag(this.inputTagName);
  }

  toggleTag(tag: string) {
    if (this.isInMyTags(tag)) {
      this.delTag(tag);
    } else {
      this.addTag(tag);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  loadTags() {
    return this.tagService.getTags(this.data.titleId).subscribe(data => {
      this.tagsLoaded = false;

      let l_tags: any[] = [];
      data.forEach(function (entry: any) {
        l_tags.push(entry.tagName);
      })
      this.tags = l_tags.slice(0, 12);

      this.tagsLoaded = true;
    })
  }

  loadMyTags() {
    return this.tagService.getUsersTags(this.data.titleId, this.data.username).subscribe(data => {
      this.myTagsLoaded = false;

      let l_myTags: any[] = [];
      data.forEach(function (entry: any) {
        l_myTags.push(entry.tagName);
      })
      this.myTags = l_myTags;

      this.myTagsLoaded = true;
    })
  }
}
