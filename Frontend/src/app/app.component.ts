import {Component} from '@angular/core';
import {AuthService} from "./service/auth.service";
import {GameService} from "./service/game.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public authService: AuthService, public gameService: GameService, private router: Router) {
  }

  userLoggedInVisible() {
    return this.authService.isLogged ? "visible" : "hidden";
  }

  userNotLoggedInVisible() {
    return this.authService.isLogged ? "hidden" : "visible";
  }

  loadGameList() {
    this.gameService.recommendations = false;
  }

  loadDashboard() {
    this.router.navigateByUrl('/dashboard');
  }

  logOut() {
    this.authService.isLogged = false;
    this.authService.username = undefined;
    this.authService.password = undefined;
    this.authService.jwt = undefined;
    this.authService.clearCookies();
    this.gameService.recommendations = false;
    this.router.navigateByUrl('/');
  }
}

