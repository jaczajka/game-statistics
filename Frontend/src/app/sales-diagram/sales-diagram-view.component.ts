import {Component} from '@angular/core';
import {GameService} from "../service/game.service";
import {AuthService} from "../service/auth.service";

@Component({
  selector: 'sales-diagram-view',
  templateUrl: './sales-diagram-view.component.html',
  styleUrls: ['./sales-diagram-view.component.scss'],
})

export class SalesDiagramViewComponent {
  NO_FILTER_DISPLAYED_STRING = "Wszystkie"//string displayed to user when we do not filter given column
  NO_FILTER_SPECIAL_STRING = ""//string passed to game service to specify that we do not want to filter by that value

  //keys used for chosenFilters map:
  RATING_MAP_KEY = "rating"
  GENRE_MAP_KEY = "genre"
  PLATFORM_MAP_KEY = "platform"
  REGION_MAP_KEY = "region"

  chosenFilters = new Map();//map that shows which filters we are currently using
  loadDiagramFromCurrentContext(){
    this.loadDiagram(
      this.chosenFilters.get(this.RATING_MAP_KEY),
      this.chosenFilters.get(this.GENRE_MAP_KEY),
      this.chosenFilters.get(this.PLATFORM_MAP_KEY),
      this.chosenFilters.get(this.REGION_MAP_KEY),
    )
  }

  //dropdowns:
  ratingNames: any = [];
  genreNames: any = [];
  platformNames: any = [];
  regionNames: any = [];

  private translateRegionName(region: String){
    let regionsTranslations = new Map();
    regionsTranslations
      .set("global", "Cały świat")
      .set("europe", "Europa")
      .set("japan", "Japonia")
      .set("northAmerica", "Ameryka północna")
      .set("other", "Reszta świata");

    return regionsTranslations.get(region);
  }

  isLoaded = false;
  resultsFound = true;
  gameList: any = [];
  sales: any = [];
  years: any = [];
  showFilters = false;

  barChartLabels: any[];
  private barChartType: string = 'line';
  private barChartLegend: boolean = true;
  private barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    spanGaps: true,
    scales: {
      xAxes: [ {
        scaleLabel: {
          display: true,
          labelString: 'Kolejne lata'
        },
      } ],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Nakład w mln sztuk'
        },
      }]
    }
  };
  barChartData: any[] = [
    { data: [], label: 'Nakład' },
  ];

  constructor(private gameService: GameService, private authService: AuthService) {
    console.log(this.authService.isLogged)
    this.chosenFilters
      .set(this.RATING_MAP_KEY, this.NO_FILTER_SPECIAL_STRING)
      .set(this.GENRE_MAP_KEY, this.NO_FILTER_SPECIAL_STRING)
      .set(this.PLATFORM_MAP_KEY, this.NO_FILTER_SPECIAL_STRING)
      .set(this.REGION_MAP_KEY, "global");
  }

  private chartClicked(e: any): void {
    console.log(e);
  }
  private chartHovered(e: any): void {
    console.log(e);
  }

  private getNewLabelsList(array: any, chosenValue: String) : Array<string> {
    let newLabelsList = new Array();

    //insert all values except chosen one
    array.forEach(function(entry: any){
      if (entry != chosenValue){
        newLabelsList.push(entry);
      }
    });

    //sort inserted values
    newLabelsList.sort();

    //finally insert chosenOne value at the beginning
    newLabelsList.unshift(chosenValue);

    return newLabelsList;
  }

  onRatingSelected(value: string) {
    this.ratingNames = this.getNewLabelsList(this.ratingNames, value);

    if (value === this.NO_FILTER_DISPLAYED_STRING) {
      value = this.NO_FILTER_SPECIAL_STRING;
    }

    this.chosenFilters.set(this.RATING_MAP_KEY, value);
    this.isLoaded = false;
    this.resultsFound = false;
    this.loadDiagramFromCurrentContext();
  }

  onGenreSelected(value: string) {
    this.genreNames = this.getNewLabelsList(this.genreNames, value);

    if (value === this.NO_FILTER_DISPLAYED_STRING) {
      value = this.NO_FILTER_SPECIAL_STRING;
    }

    this.chosenFilters.set(this.GENRE_MAP_KEY, value);
    this.isLoaded = false;
    this.resultsFound = false;
    this.loadDiagramFromCurrentContext();
  }

  onPlatformSelected(value: string) {
    this.platformNames = this.getNewLabelsList(this.platformNames, value);

    if (value === this.NO_FILTER_DISPLAYED_STRING) {
      value = this.NO_FILTER_SPECIAL_STRING;
    }

    this.chosenFilters.set(this.PLATFORM_MAP_KEY, value);
    this.isLoaded = false;
    this.resultsFound = false;
    this.loadDiagramFromCurrentContext();
  }

  onRegionSelected(value: string) {
    this.regionNames = this.getNewLabelsList(this.regionNames, value);

    if (value === this.NO_FILTER_DISPLAYED_STRING) {
      value = this.NO_FILTER_SPECIAL_STRING;
    }

    this.chosenFilters.set(this.REGION_MAP_KEY, value);
    this.isLoaded = false;
    this.resultsFound = false;
    this.loadDiagramFromCurrentContext();
  }

  loadDiagram(rating: string = "", genre: string = "", platform: string = "", region: string = "") {
    this.gameService.getSalesDiagram(rating, genre, platform, region).subscribe((data: {}) => {
      this.gameList = data;

      let yearLabels = new Set();
      let sales = new Array();

      this.gameList.forEach(function(entry: any) {
        if(entry.sales !== null) {
          yearLabels.add(entry.yearOfRelease);
          sales.push(entry.value);
        }
      });

      this.years = yearLabels;
      this.sales = sales.map(x => x.toFixed(2));

      this.barChartLabels = Array.from(yearLabels);
      this.barChartData[0] = { data: this.sales, backgroundColor: "rgb(244,89,42,0.1)",
        borderColor: "#F4592A", pointBackgroundColor:"#F4592A", pointHoverBorderColor: "#F4592A",
        label: 'Nakład w mln sztuk' };
      this.isLoaded = true;

      if (this.sales.length > 0) {
        this.resultsFound = true;
      }

    })
  }

  initDropdowns() {
    this.gameService.getNames('/ratings').subscribe((data: {}) => {
      this.ratingNames = data;
      this.ratingNames.unshift(this.NO_FILTER_DISPLAYED_STRING);
    });

    this.gameService.getNames('/genres').subscribe((data: {}) => {
      this.genreNames = data;
      this.genreNames.unshift(this.NO_FILTER_DISPLAYED_STRING);
    });

    this.gameService.getNames('/platforms').subscribe((data: {}) => {
      this.platformNames = data;
      this.platformNames.unshift(this.NO_FILTER_DISPLAYED_STRING);
    });

    this.gameService.getNames('/regions').subscribe((data: {}) => {
      this.regionNames = data;
      this.regionNames = this.getNewLabelsList(this.regionNames, "global");
    });
  }

  ngOnInit() {
    this.initDropdowns();
    this.loadDiagram();
  };
}
