import {Component} from '@angular/core';
import {GameService} from "../service/game.service";
import {Game} from "../model/game";
import {ChartsModule} from 'ng2-charts/ng2-charts';

@Component({
  selector: 'annual-diagram-view',
  templateUrl: './annual-diagram-view.component.html',
  styleUrls: ['./annual-diagram-view.component.scss']
})
export class AnnualDiagramViewComponent {
  NO_FILTER_DISPLAYED_STRING = "Wszystkie"//string displayed to user when we do not filter given column
  NO_FILTER_SPECIAL_STRING = ""//string passed to game service to specify that we do not want to filter by that value

  //keys used for chosenFilters map:
  RATING_MAP_KEY = "rating"
  GENRE_MAP_KEY = "genre"

  chosenFilters = new Map();//map that shows which filters we are currently using
  loadDiagramFromCurrentContext(){
    this.loadDiagram(
      this.chosenFilters.get(this.RATING_MAP_KEY),
      this.chosenFilters.get(this.GENRE_MAP_KEY),
    )
  }
  showFilters = false;
  isLoaded = false;
  resultsFound = true;
  gameList: any = [];

  years: any = [];
  userScores: any = [];
  criticScores: any = [];

  ratingNames: any = [];
  genreNames: any = [];

  private barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    spanGaps: true,
    scales: {
      xAxes: [ {
        scaleLabel: {
          display: true,
          labelString: 'Kolejne lata'
        },
      } ],
      yAxes: [{
        id: 'critics',
        type: 'linear',
        position: 'left',
        scaleLabel: {
          display: true,
          labelString: 'Ocena krytyków'
        },
        ticks: {
          beginAtZero: true,
          max: 10,
        },
      }, {
        id: 'users',
        type: 'linear',
        position: 'right',
        scaleLabel: {
          display: true,
          labelString: 'Ocena użytkowników'
        },
        ticks: {
          beginAtZero: true,
          max: 10,
        }
      }]
    }
  };

  barChartLabels: any[];
  private barChartType: string = 'line';
  private barChartLegend: boolean = true;

  barChartData: any[] = [
    { data: [], label: 'Ocena krytyków' },
    { data: [], label: 'Ocena użytkowników' }
  ];

  // events
  private chartClicked(e: any): void {
      console.log(e);
  }

  private chartHovered(e: any): void {
      console.log(e);
  }

  private getNewLabelsList(array: any, chosenValue: String) : Array<string> {
    let newLabelsList = new Array();

    //insert all values except chosen one
    array.forEach(function(entry: any){
      if (entry != chosenValue){
        newLabelsList.push(entry);
      }
    });

    newLabelsList.sort();
    newLabelsList.unshift(chosenValue);

    return newLabelsList;
  }

  onRatingSelected(value: string) {
    this.ratingNames = this.getNewLabelsList(this.ratingNames, value);

    if (value === this.NO_FILTER_DISPLAYED_STRING) {
      value = this.NO_FILTER_SPECIAL_STRING;
    }

    this.chosenFilters.set(this.RATING_MAP_KEY, value);
    this.isLoaded = false;
    this.resultsFound = false;
    this.loadDiagramFromCurrentContext();
  }

  onGenreSelected(value: string) {
    this.genreNames = this.getNewLabelsList(this.genreNames, value);

    if (value === this.NO_FILTER_DISPLAYED_STRING) {
      value = this.NO_FILTER_SPECIAL_STRING;
    }

    this.chosenFilters.set(this.GENRE_MAP_KEY, value);
    this.isLoaded = false;
    this.resultsFound = false;
    this.loadDiagramFromCurrentContext();
  }

  constructor(private gameService: GameService) {
    this.chosenFilters
      .set(this.RATING_MAP_KEY, this.NO_FILTER_SPECIAL_STRING)
      .set(this.GENRE_MAP_KEY, this.NO_FILTER_SPECIAL_STRING);
  }

  loadDiagram(rating: string = "", genre: string = "") {
    this.gameService.getAnnualGrades(rating, genre).subscribe( (data: {}) => {
      this.gameList = data;

      let yearLabels = new Set();
      let userData = new Array();
      let criticData = new Array();

      this.gameList.forEach(function(entry: any){

        if(entry.userScore !== null && entry.criticScore !== null) {
          yearLabels.add(entry.year);
          userData.push(entry.userScore);
          criticData.push(entry.criticScore);
        }
      });

      this.years = yearLabels;
      this.userScores = userData.map(x => x.toFixed(2));
      this.criticScores = criticData.map(x => x/10).map(x => x.toFixed(2));

      this.barChartLabels = Array.from(yearLabels);
      this.barChartData[0] = { data: this.userScores, backgroundColor: "rgb(255,171,15,0.1)",
        borderColor: "#FFAB0F", pointBackgroundColor:"#FFAB0F", pointHoverBorderColor: "#FFAB0F",
        label: 'Ocena użytkowników', yAxisID: 'users'};
      this.barChartData[1] = { data: this.criticScores, backgroundColor: "rgb(7,127,7,0.1)",
        borderColor: "#077F07", pointBackgroundColor:"#077F07", pointHoverBorderColor: "#077F07",
        label: 'Ocena krytyków', yAxisID: 'critics'};
      this.isLoaded = true;

      if (this.userScores.length > 0 || this.criticScores.length > 0) {
        this.resultsFound = true;
      }
    })
  }

  initDropdowns() {
    this.gameService.getNames('/ratings').subscribe((data: {}) => {
      this.ratingNames = data;
      this.ratingNames.unshift(this.NO_FILTER_DISPLAYED_STRING);
    });

    this.gameService.getNames('/genres').subscribe((data: {}) => {
      this.genreNames = data;
      this.genreNames.unshift(this.NO_FILTER_DISPLAYED_STRING);
    });
  }

  ngOnInit() {
    this.initDropdowns();
    this.loadDiagram();
  }
}
