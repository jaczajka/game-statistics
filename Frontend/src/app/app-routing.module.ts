import {RouterModule, Routes} from "@angular/router";

import {GameDetailsViewComponent} from './game-details/game-details-view.component';
import {GameListViewComponent} from './game-list/game-list-view.component';
import {LoginPageComponent} from './user/login-page.component';
import {RegisterPageComponent} from './user/register-page.component';
import {AnnualDiagramViewComponent} from './annual-diagram/annual-diagram-view.component';
import {SalesDiagramViewComponent} from './sales-diagram/sales-diagram-view.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PageNotFoundComponent} from './common/page-not-found-component';
import {NgModule} from '@angular/core';

const routes: Routes = [
  { path: '', redirectTo: '/game-list', pathMatch: 'full' },
  { path: 'game-list', component: GameListViewComponent },
  { path: 'game-details/:id', component: GameDetailsViewComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'register', component: RegisterPageComponent },
  { path: 'annual-diagram', component: AnnualDiagramViewComponent },
  { path: 'sales-diagram', component: SalesDiagramViewComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: "reload"})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
