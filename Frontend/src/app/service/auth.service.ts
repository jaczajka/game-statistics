import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {GenericService} from "./generic.service";
import {CookieService} from "ngx-cookie-service"

@Injectable({
  providedIn: 'root'
})
export class AuthService extends GenericService{
  private static URL = '';

  isLogged: boolean;
  username: string;
  password: string;
  jwt: string;

  loadStateFromCookies(){
    //if cookies are empty
    if (this.cookieService.get('username') !== ''
      || this.cookieService.get('password') !== ''
      || this.cookieService.get('jwt') !== ''
    ){
      this.isLogged = true;
      this.username = this.cookieService.get('username');
      this.password = this.cookieService.get('password');
      this.jwt = this.cookieService.get('jwt');
    }
    else{
      this.isLogged = false;
    }
  }

  saveStateToCookies(){
    this.cookieService.set('username', this.username);
    this.cookieService.set('password', this.password);
    this.cookieService.set('jwt', this.jwt);
    this.isLogged = true;
  }

  clearCookies(){
    this.cookieService.delete('username');
    this.cookieService.delete('password');
    this.cookieService.delete('jwt');
    this.isLogged = false;
  }

  constructor(client: HttpClient, private cookieService: CookieService) {
    super(client, AuthService.URL);
    this.loadStateFromCookies();
  }

  setIsLogged(val: boolean){
    this.isLogged = val;
  }
  login(username: string, password: string){
     return this.client.post<any>(this.url + "authenticate", {"username": username, "password": password});
  }

  register(username: string, password: string){
    return this.client.post<any>(this.url + "register", {"username": username, "password": password});
  }
}
