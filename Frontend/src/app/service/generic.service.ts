import {HttpClient, HttpHeaders} from "@angular/common/http";
import {CONFIG} from "../../environment";

export abstract class GenericService {

  protected url: string = null;
  protected httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  protected constructor(protected client: HttpClient, urlPattern: string) {
    this.url = CONFIG.API_URL + '/' + urlPattern;
  }
}
