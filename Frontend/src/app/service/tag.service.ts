import {GenericService} from "./generic.service";
import {Injectable} from "@angular/core";
import {HttpClient, HttpParams, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Page} from "../model/page";
import {Tag} from "../model/tag";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class TagService extends GenericService {

  private static URL = 'tags';

  constructor(client: HttpClient) {
    super(client, TagService.URL);
  }

  getTags(titleId: number): Observable<any> {
      return this.client.get<Tag>(this.url + "/" + titleId, {}).pipe();
  }

  getUsersTags(titleId: number, username: string): Observable<any> {
    return this.client.get<Tag>(this.url + "/" + titleId + "/" + username, {}).pipe()
  }

  postUsersTag(titleId: number, username: string, tagName: string, token: string): Observable<any> {
    const headers = {'Authorization': 'Bearer ' + token};
    return this.client.post<any>(this.url  + "/" + titleId + "/" + username + "?" + "tagName=" + tagName, {}, {headers}).pipe();
  }

  deleteUsersTag(titleId: number, username: string, tagName: string, token: string): Observable<any> {
    const headers = {'Authorization': 'Bearer ' + token};
    return this.client.delete<any>(this.url  + "/" + titleId + "/" + username + "?" + "tagName=" + tagName, {headers}).pipe();
  }
}
