import {GenericService} from "./generic.service";
import {Injectable} from "@angular/core";
import {HttpClient, HttpParams, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {GameListFilters} from "../game-list/filters/game-list-filters";
import {ClientScoresFilters} from "../dashboard/filters/client-scores-filters";
import {Page} from "../model/page";
import {ClientScoresPage} from "../model/client-scores-page";
import {Title} from "../model/title";
import {MinMaxScores} from "../model/min-max-scores";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class GameService extends GenericService {

  private static URL = 'games';
  recommendations: boolean = false;

  constructor(client: HttpClient, private authService: AuthService) {
    super(client, GameService.URL);
  }

  getList(filters: GameListFilters): Observable<Page> {
    let params = new HttpParams();
    Object.entries(filters).forEach(([filter, value]) => params = params.append(filter, value));
    return this.client.get<Page>(this.url + (this.recommendations ? ("/recommendations/" + this.authService.username) : ""), {params}).pipe()
  }

  getClientScores(filters: ClientScoresFilters): Observable<ClientScoresPage> {
    let params = new HttpParams();
    Object.entries(filters).forEach(([filter, value]) => params = params.append(filter, value));
    return this.client.get<ClientScoresPage>(this.url + "/client-scores/" + this.authService.username).pipe();
  }

  getClientFavourites(filters: ClientScoresFilters): Observable<Page> {
    let params = new HttpParams();
    Object.entries(filters).forEach(([filter, value]) => params = params.append(filter, value));
    return this.client.get<Page>(this.url + "/favourites/" + this.authService.username).pipe();
  }

  getOne(id: number): Observable<Title> {
    return this.client.get<Title>(this.url + '/' + id).pipe();
  }

  getAnnualGrades(rating: string = "", genre: string = ""): Observable<any> {
    let ratingURL = (rating === "") ? "" : 'rating=' + rating + '&';
    let genreURL = (genre === "") ? "" : 'genre=' + genre + '&';

    return this.client.get<any>(this.url + '/annualGrades?' + ratingURL + genreURL).pipe();
  }

  getSalesDiagram(rating: string = "", genre: string = "", platform: string = "", region: string = ""):
    Observable<any> {
    let ratingURL = (rating === "") ? "" : 'rating=' + rating + '&';
    let genreURL = (genre === "") ? "" : 'genre=' + genre + '&';
    let platformURL = (platform === "") ? "" : 'platform=' + platform + '&';
    let regionURL = (region === "") ? "" : 'region=' + region + '&';

    return this.client.get<any>(
      this.url + '/diagrams?' + ratingURL + genreURL + platformURL + regionURL
    ).pipe();
  }

  getNames(type: string): Observable<any> {
    return this.client.get<any>(this.url + type + '/names').pipe();
  }

  getUserScore(username: string, gameId: number): Observable<any> {
    let gameURL = 'gameId=' + gameId + '&';
    let userURL = 'username=' + username + '&';
    return this.client.get<any>(this.url + '/client-scores?' + gameURL + userURL).pipe();
  }


  postUserScore(scoreValue: number, username: string, gameId: number, token: string): Observable<any> {
    const headers = {'Authorization': 'Bearer ' + token};
    let scoreURL = 'value=' + scoreValue + '&';
    let gameURL = 'gameId=' + gameId + '&';
    let userURL = 'username=' + username + '&';
    return this.client.post<any>(this.url + '/client-scores?' + scoreURL + gameURL + userURL, {}, {headers}).pipe();
  }

  getUserFavourite(username: string, titleId: number): Observable<any> {
    let titleURL = 'titleId=' + titleId + '&';
    let userURL = 'username=' + username + '&';
    return this.client.get<any>(this.url + '/favourites?' + titleURL + userURL).pipe();
  }

  postUserFavourite(username: string, titleId: number, token: string): Observable<any> {
    const headers = {'Authorization': 'Bearer ' + token};
    let userURL = 'username=' + username + '&';
    let titleURL = 'titleId=' + titleId + '&';
    return this.client.post<any>(this.url + '/favourites?' + titleURL + userURL, {}, {headers}).pipe();
  }

  getFavouriesCount(username: string) : Observable<number> {
    let userURL = 'username=' + username;
    return this.client.get<number>(this.url + '/favourites/user-favourites-count?' + userURL).pipe();
  }

  getFavouritesRatio(titleId: number): Observable<any> {
    let titleURL = 'titleId=' + titleId + '&';
    return this.client.get<any>(this.url + '/favourites/ratio?' + titleURL).pipe();
  }

  getSimilarityDiagram(column: string, titleId: number): Observable<any> {
    return this.client.get<any>(this.url + '/diagrams/similarTitles/' + column + '/' + titleId).pipe();
  }

  getMinMaxScores(username: string) : Observable<MinMaxScores> {
    let userURL = 'username=' + username;
    return this.client.get<MinMaxScores>(this.url + '/min-max-scores?' +  userURL).pipe();
  }
}
