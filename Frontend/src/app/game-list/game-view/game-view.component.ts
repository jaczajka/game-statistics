import {Component, Input} from "@angular/core";
import {GameListEntry} from "../../model/game-list-entry";

@Component({
  selector: 'game-view',
  templateUrl: './game-view.component.html',
  styleUrls: ['./game-view.component.scss']
})
export class GameViewComponent {
  @Input() gameEntry: GameListEntry;

  get developers() {
    return this.gameEntry.developers.replace("Not known", "nazwa firmy nieznana");
  }
}
