import {Component} from '@angular/core';
import {GameService} from "../service/game.service";
import {ActivatedRoute, Router} from "@angular/router";
import {GameListFilters} from "./filters/game-list-filters";
import {Page} from "../model/page";

@Component({
  selector: 'game-list-view',
  templateUrl: './game-list-view.component.html',
  styleUrls: ['./game-list-view.component.scss']
})
export class GameListViewComponent {

  gameList: any = [];
  filters: GameListFilters = new GameListFilters();
  lastPage: number = 1;
  loaded = false;

  private PER_PAGE = 20;

  constructor(private gameService: GameService, private route: ActivatedRoute, private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      Object.assign(this.filters, params);
      this.filters.perPage = this.PER_PAGE;
      this.loadGameList();
    });
  }

  handleFiltersEvent = (newFilters: Object) => {
    Object.assign(this.filters, newFilters);
    this.filters.page = 1;
    this.filters.minScoresCount = this.filters.minScoresCount != 0 ? this.filters.minScoresCount : undefined;
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: this.filters
    });
  };

  handlePaginationEvent = (page: number) => {
    if (page != this.filters.page) {
      this.filters.page = page;
      this.router.navigate([], {relativeTo: this.route, queryParams: this.filters});
    }
  };

  loadGameList = () => {
    this.loaded = false;
    console.log(this.filters);
    return this.gameService.getList(this.filters).subscribe((data: Page) => {
      this.gameList = data.items;
      this.lastPage = Math.ceil(data.totalCount / this.PER_PAGE);
      this.loaded = true;
    })
  };

}
