import {Component, EventEmitter, Input, Output} from "@angular/core";
import {GameListFilters} from "./game-list-filters";
import {GameService} from "../../service/game.service";
import {AuthService} from "../../service/auth.service";

@Component({
  selector: 'game-filters',
  templateUrl: './game-filters.component.html',
  styleUrls: ['../game-list-view.component.scss']
})
export class GameFiltersComponent {
  @Input() filters: GameListFilters;
  @Output() filterEvent: EventEmitter<Object> = new EventEmitter();

  constructor(public gameService: GameService, public authService: AuthService) {
  }

  showRecommendations(flag: boolean) {
    this.gameService.recommendations = flag;
    this.filters.recommendations = flag;
    this.filterEvent.emit(this.filters);
  }

  clearFilters = () => {
    this.filters.phrase = undefined;
    this.filters.genre = undefined;
    this.filters.developers = undefined;
    this.filters.minScoresCount = undefined;
    this.filters.minUsersScore = undefined;
    this.filters.maxUsersScore = undefined;
    this.filters.minCriticsScore = undefined;
    this.filters.maxCriticsScore = undefined;
    this.filters.sortBy = ['title', 'asc'];
    this.filterEvent.emit(this.filters)
  }
}
