export class GameListFilters {
  phrase: string;
  genre: string;
  developers: string;
  minScoresCount: number;
  minUsersScore: number;
  maxUsersScore: number;
  minCriticsScore: number;
  maxCriticsScore: number;
  sortBy: Array<string> = ['title', 'asc'];
  page: number = 1;
  perPage: number = 10;
  recommendations: boolean = false;
}
