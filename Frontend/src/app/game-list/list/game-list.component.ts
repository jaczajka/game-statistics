import {Component, Input} from "@angular/core";
import {GameListEntry} from "../../model/game-list-entry";

@Component({
  selector: 'game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['../game-list-view.component.scss']
})
export class GameListComponent {
  @Input() games: Array<GameListEntry> = [];
}
