import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'sorter',
  templateUrl: './sorter.component.html',
})
export class SorterComponent {
  @Input() label: string;
  @Input() fieldName: string;
  @Input() sortBy: Array<string>;
  @Output() sortByChange: EventEmitter<Array<string>> = new EventEmitter();

  private sortingDirection: string;

  private ASCENDING = 'asc';
  private DESCENDING = 'desc';

  ngOnInit() {
    const index = this.sortBy.indexOf(this.fieldName);
    this.sortingDirection = index >= 0 ? this.sortBy[index + 1] : undefined;
  }

  sortingChange = () => {
    if (this.sortingDirection === undefined) {
      this.sortingDirection = this.ASCENDING;
      this.sortBy.push(this.fieldName, this.sortingDirection);
    } else if (this.sortingDirection == this.ASCENDING) {
      this.sortingDirection = this.DESCENDING;
      this.sortBy[this.sortBy.indexOf(this.fieldName) + 1] = this.sortingDirection;
    } else {
      this.sortingDirection = undefined;
      this.sortBy.splice(this.sortBy.indexOf(this.fieldName), 2);
    }
    this.sortByChange.emit(this.sortBy);
  };

  get direction() {
    if (this.sortingDirection == this.ASCENDING) {
      return "&#8595;";
    } else if (this.sortingDirection == this.DESCENDING) {
      return "&#8593;";
    }
    return 'x';
  }
}
