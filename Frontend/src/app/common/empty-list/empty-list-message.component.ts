import {Component, Input} from '@angular/core';

@Component({
  selector: 'empty-list-message',
  templateUrl: './empty-list-message.component.html',
  styleUrls: ['./empty-list-message.component.scss']
})
export class EmptyListMessageComponent {
  @Input() message: string = 'Nic tu nie ma :(';
  @Input() hint: string = 'Spróbuj zmienić filtry';
  @Input() big: boolean = true;
}
