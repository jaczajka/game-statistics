import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {fromEvent} from "rxjs";
import {map} from "rxjs/operators";

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {
  @Input() currentPage: number = 1;
  @Input() lastPage: number;
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();

  private MAX_PAGES = 10;

  next = () => this.handlePageEvent(Number(this.currentPage) + 1);
  prev = () => this.handlePageEvent(Number(this.currentPage) - 1);
  first = () => this.handlePageEvent(1);
  last = () => this.handlePageEvent(Number(this.lastPage));

  handlePageEvent = (page: number) => {
    if (page != this.currentPage) {
      this.currentPage = page;
      this.pageChange.emit(page);
    }
    return true;
  };

  get pageNumbers() {
    const base = Math.floor((this.currentPage - 1) / 10);
    const numberOfPages = this.MAX_PAGES > this.lastPage || this.currentPage == this.lastPage
      ? this.lastPage % 10
      : this.MAX_PAGES;
    return Array(numberOfPages).fill(0).map((x, i) => i + base * 10 + 1);
  }
}
