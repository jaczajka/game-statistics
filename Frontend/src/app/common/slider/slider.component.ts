import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent {
  @Input() id: string;
  @Input() label: string;
  @Input() minValue: number = 1;
  @Input() maxValue: number = 10;
  @Input() step: number = 1;
  @Input() value: number;
  @Output() valueChange: EventEmitter<number> = new EventEmitter();

  valueChanged = (value: number) => {
    this.value = value <= this.maxValue ? value : undefined;
    this.valueChange.emit(this.value);
  }
}
