import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ChartsModule} from 'ng2-charts';

import {GameDetailsViewComponent} from './game-details/game-details-view.component';
import {GameListViewComponent} from './game-list/game-list-view.component';
import {LoginPageComponent} from './user/login-page.component';
import { RegisterPageComponent } from './user/register-page.component';
import {PageNotFoundComponent} from './common/page-not-found-component';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {GameService} from "./service/game.service";
import {GameViewComponent} from "./game-list/game-view/game-view.component";
import {GameListComponent} from "./game-list/list/game-list.component";
import {GameFiltersComponent} from "./game-list/filters/game-filters.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ClientScoresViewComponent} from "./dashboard/client-scores-view/client-scores-view.component";
import {ClientScoresListComponent} from "./dashboard/client-scores-list/client-scores-list.component";
import {SliderComponent} from "./common/slider/slider.component";
import {RatingModule} from 'ng-starrating'
import {SorterComponent} from "./common/sorter/sorter.component";
import {PaginationComponent} from "./common/pagination/pagination.component";
import {AnnualDiagramViewComponent} from "./annual-diagram/annual-diagram-view.component";
import {SalesDiagramViewComponent} from './sales-diagram/sales-diagram-view.component';
import {FormsModule} from "@angular/forms";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {TagsDialogComponent} from "./game-details/tags_view_dialog/tags-dialog.component";
import {MAT_DIALOG_DATA, MatDialogModule} from "@angular/material/dialog";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {SimilarTitlesDiagramComponent} from "./game-details/similar-titles-diagram/similar-titles-diagram.component";
import {EmptyListMessageComponent} from "./common/empty-list/empty-list-message.component";
import '@angular/flex-layout'

@NgModule({
  declarations: [
    AppComponent,
    GameDetailsViewComponent,
    GameListViewComponent,
    LoginPageComponent,
    RegisterPageComponent,
    PageNotFoundComponent,
    GameViewComponent,
    GameListComponent,
    GameFiltersComponent,
    SliderComponent,
    SorterComponent,
    PaginationComponent,
    AnnualDiagramViewComponent,
    SalesDiagramViewComponent,
    SimilarTitlesDiagramComponent,
    TagsDialogComponent,
    DashboardComponent,
    ClientScoresViewComponent,
    ClientScoresListComponent,
    EmptyListMessageComponent,
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RatingModule,
    ChartsModule,
    FormsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    BrowserAnimationsModule,
  ],
  providers: [GameService],
  bootstrap: [AppComponent],
  entryComponents: [TagsDialogComponent]
})
export class AppModule {
}
