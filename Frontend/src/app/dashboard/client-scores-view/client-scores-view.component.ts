import {Component, Input} from "@angular/core";
import {ClientScoresEntry} from "../../model/client-scores-entry";

@Component({
  selector: 'client-scores-view',
  templateUrl: './client-scores-view.component.html',
  styleUrls: ['./client-scores-view.component.scss']
})
export class ClientScoresViewComponent {
  @Input() clientScoresEntry: ClientScoresEntry;

  get developers() {
    return this.clientScoresEntry.developers.replace("Not known", "nazwa firmy nieznana");
  }
}
