import {Component, Input} from "@angular/core";
import {ClientScoresEntry} from "../../model/client-scores-entry";

@Component({
  selector: 'client-scores-list',
  templateUrl: './client-scores-list.component.html',
  styleUrls: ['../dashboard.component.scss']
})
export class ClientScoresListComponent {
  @Input() clientScores: Array<ClientScoresEntry> = [];
}
