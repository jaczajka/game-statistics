import {Component} from '@angular/core';
import {AuthService} from "../service/auth.service";
import {GameService} from "../service/game.service";
import {ActivatedRoute, Router} from "@angular/router";
import {GameListFilters} from "../game-list/filters/game-list-filters";
import {GameListEntry} from "../model/game-list-entry";
import {ClientScoresEntry} from "../model/client-scores-entry";
import {ClientScoresFilters} from "./filters/client-scores-filters";
import {Page} from "../model/page";
import {ClientScoresPage} from "../model/client-scores-page";
import {MinMaxScores} from "../model/min-max-scores";

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  gameList: GameListEntry[] = [];
  favouritesList: GameListEntry[] = [];
  clientScoresList: ClientScoresEntry[] = [];
  filters: GameListFilters = new GameListFilters();
  clientScoresFilters: ClientScoresFilters = new ClientScoresFilters();
  favouritesFilters: ClientScoresFilters = new ClientScoresFilters();
  minScore: number;
  maxScore: number;
  favouritesCount: number;
  lastPage: number = 1;
  tabs: string[] = ["Polecane", "Ocenione", "Ulubione"];
  currentTab: string = "Polecane";
  loaded = false;

  private PER_PAGE = 10;

  constructor(private gameService: GameService, private route: ActivatedRoute,
              private router: Router, public authService: AuthService) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.gameService.recommendations = true;
    this.filters.recommendations = true;
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      Object.assign(this.filters, params);
      this.filters.perPage = this.PER_PAGE;
      this.clientScoresFilters.perPage = this.PER_PAGE;
      this.favouritesFilters.perPage = this.PER_PAGE;
      this.loadGameList();
      this.loadMinMaxScores();
      this.loadFavouritesCount();
    });
  }

  handlePaginationEvent = (page: number) => {
    switch (this.currentTab) {
    case "Polecane":
      if (page != this.filters.page) {
        this.filters.page = page;
        this.loadGameList();
      }
      break;
    case "Ocenione":
      if (page != this.clientScoresFilters.page) {
        this.clientScoresFilters.page = page;
        this.loadClientScoresList();
      }
      break;
    case "Ulubione":
      if (page != this.favouritesFilters.page) {
        this.favouritesFilters.page = page;
        this.loadClientFavouritesList();
      }
      break;
    }
  };

  loadGameList = () => {
    this.loaded = false;
    return this.gameService.getList(this.filters).subscribe((data: Page) => {
      this.gameList = data.items;
      this.lastPage = Math.ceil(data.totalCount / this.PER_PAGE);
      this.loaded = true;
    })
  };

  loadClientScoresList = () => {
    this.loaded = false;
    return this.gameService.getClientScores(this.clientScoresFilters).subscribe((data: ClientScoresPage) => {
      this.clientScoresList = data.items;
      this.lastPage = Math.ceil(this.clientScoresList.length / this.PER_PAGE);
      this.loaded = true;
    })
  };

  loadClientFavouritesList = () => {
    this.loaded = false;
    return this.gameService.getClientFavourites(this.favouritesFilters).subscribe((data: Page) => {
      this.favouritesList = data.items;
      this.lastPage = Math.ceil(this.favouritesList.length / this.PER_PAGE);
      this.loaded = true;
    })
  }

  loadMinMaxScores() {
    return this.gameService.getMinMaxScores(this.authService.username)
	                   .subscribe((scores: MinMaxScores) => {
      this.minScore = scores.minScore;
      this.maxScore = scores.maxScore;
    })
  };

  loadFavouritesCount() {
    return this.gameService.getFavouriesCount(this.authService.username)
                           .subscribe((count: number) => {
      this.favouritesCount = count;
    })
  };

  switchTab(tab: string) {
    if (this.currentTab === tab) {
      return;
    }
    switch (tab) {
    case "Polecane":
      this.filters.page = 1;
      this.loadGameList();
      break;
    case "Ocenione":
      this.clientScoresFilters.page = 1;
      this.loadClientScoresList();
      break;
    case "Ulubione":
      this.favouritesFilters.page = 1;
      this.loadClientFavouritesList();
      break;
    }
    this.currentTab = tab;
  }

  isTabActive(tab: string) : boolean {
    return this.currentTab === tab;
  }
}
