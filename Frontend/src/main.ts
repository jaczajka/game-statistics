import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {AppModule} from './app/app.module';
import {enableProdMode} from "@angular/core";
import {CONFIG} from "./environment";

if (CONFIG['MODE'] == 'production') {
  enableProdMode();
}
platformBrowserDynamic().bootstrapModule(AppModule);
