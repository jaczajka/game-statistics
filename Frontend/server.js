const path = require("path");
const express = require("express");

const DIST_DIR = path.join(__dirname, "dist");
console.log("Setting source dir to " + DIST_DIR);
const app = express();
app.use(express.static(DIST_DIR));
app.get("*", (req, res) => res.sendFile(path.join(DIST_DIR, "index.html")));
app.set('port', (process.env.PORT || 5000));
app.listen(app.get('port'), () =>
  console.log('Node server is running on port ' + app.get('port'))
);
