'use strict';

const webpack = require('webpack');
const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const helpers = require('./helpers');
const isDev = process.env.NODE_ENV !== 'production';

module.exports = {
  entry: {
    vendor: './src/vendor.ts',
    polyfills: './src/polyfills.ts',
    main: './src/main.ts',
  },

  resolve: {
    extensions: ['.ts', '.js', '.scss']
  },

  module: {
    rules: [
      {
        test: /\.html$/,
        loader: 'html-loader',
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader',
        ],
      },
      {
        test: /\.(scss|sass)$/,
        use: [
          {loader: 'style-loader', options: {sourceMap: isDev}},
          {loader: 'css-loader', options: {sourceMap: isDev}},
          {loader: 'sass-loader', options: {sourceMap: isDev}}
        ],
        include: helpers.root('src', 'assets')
      },
      {
        test: /\.(scss|sass)$/,
        use: [
          'to-string-loader',
          {loader: 'css-loader', options: {sourceMap: isDev}},
          {loader: 'sass-loader', options: {sourceMap: isDev}}
        ],
        include: helpers.root('src', 'app')
      },
      {
        test: /\.(png|jp(e*)g|svg)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 26000,
          }
        }]
      },
    ]
  },

  plugins: [
    new CleanWebpackPlugin({root: helpers.root("dist"), verbose: true}),
    new HtmlWebpackPlugin({template: 'src/index.html'}),
    new webpack.ContextReplacementPlugin(/\@angular(\\|\/)core(\\|\/)fesm5/, path.join(__dirname, './src')),
    new CopyWebpackPlugin([
      {from:'src/assets/images',to:'images'},
      {from:'favicon.png',to:'favicon.png'},
    ]),
  ]
};
