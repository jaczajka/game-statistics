#!/bin/bash

PSQLDIR=$(find / -name PG_VERSION)
[ -n "$PSQLDIR" ]                                      \
  && PSQLDIR=$(echo $PSQLDIR                           \
               | head -n 1                             \
               | sed -e 's/postgresql.*/postgresql/g')
PSQLCONF=$(find / -name pg_hba.conf)
[ -n "$PSQLCONF" ]             \
  && PSQLCONF=$(echo $PSQLCONF \
                | head -n 1)

if [ -n "$PSQLDIR" -a -n "$PSQLCONF" ]
then
    chown -R postgres:postgres $PSQLDIR
    chmod -R u=rwX,go= $PSQLDIR
    sed -ie 's/peer/trust/g' $PSQLCONF
    sed -ie 's/md5/trust/g' $PSQLCONF
    service postgresql restart

    export PGPASSWORD=$POSTGRES_PASSWORD
    createdb -U $POSTGRES_USER $POSTGRES_DB
fi

exit 0
